package pinlockview;

import android.animation.LayoutTransition;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.support.annotation.IntDef;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.signidapp.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * It represents a set of indicator dots which when attached with {@link PinLockView}
 * can be used to indicate the current length of the input
 * <p>
 * Created by aritraroy on 01/06/16.
 */
public class IndicatorDots extends LinearLayout {
public static int LENGTH;
public static int clcikedDelete=0;
    @IntDef({IndicatorType.FIXED, IndicatorType.FILL, IndicatorType.FILL_WITH_ANIMATION})
    @Retention(RetentionPolicy.SOURCE)
    public @interface IndicatorType {
        int FIXED = 0;
        int FILL = 1;
        int FILL_WITH_ANIMATION = 2;
    }

    private static final int DEFAULT_PIN_LENGTH = 4;
public static boolean nopassword=false;
    private int mDotDiameter;
    private int mDotSpacing;
    private int mFillDrawable;
    private int mEmptyDrawable;
    private int mPinLength;
    private int mIndicatorType;

    private int mPreviousLength;

    public IndicatorDots(Context context) {
        this(context, null);
    }

    public IndicatorDots(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IndicatorDots(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PinLockView);

        try {
            mDotDiameter = (int) typedArray.getDimension(R.styleable.PinLockView_dotDiameter, ResourceUtils.getDimensionInPx(getContext(), R.dimen.default_dot_diameter));
            mDotSpacing = (int) typedArray.getDimension(R.styleable.PinLockView_dotSpacing, ResourceUtils.getDimensionInPx(getContext(), R.dimen.default_dot_spacing));
            mFillDrawable = typedArray.getResourceId(R.styleable.PinLockView_dotFilledBackground,
                    R.drawable.circle);

            mEmptyDrawable = typedArray.getResourceId(R.styleable.PinLockView_dotEmptyBackground,
                    R.drawable.greencircle);
            mPinLength = typedArray.getInt(R.styleable.PinLockView_pinLength, DEFAULT_PIN_LENGTH);
            mIndicatorType = typedArray.getInt(R.styleable.PinLockView_indicatorType,
                    IndicatorType.FIXED);


        } finally {
            typedArray.recycle();
        }

        initView(context);
    }

    public void initView(Context context) {
        ViewCompat.setLayoutDirection(this, ViewCompat.LAYOUT_DIRECTION_LTR);
        if (mIndicatorType == 0) {
            for (int i = 0; i < mPinLength; i++) {
                View dot = new View(context);
                emptyDot(dot);

                LayoutParams params = new LayoutParams(mDotDiameter,
                        mDotDiameter);
                params.setMargins(mDotSpacing, 0, mDotSpacing, 0);
                dot.setLayoutParams(params);

                addView(dot);
            }
        } else if (mIndicatorType == 2) {

            for (int i = 0; i < 4; i++) {
                View dot = new View(context);
                emptyDot(dot);

                LayoutParams params = new LayoutParams(mDotDiameter,
                        mDotDiameter);
                params.setMargins(mDotSpacing, 0, mDotSpacing, 0);
                dot.setLayoutParams(params);

                addView(dot);
                 setLayoutTransition(new LayoutTransition());
            }
          //  setLayoutTransition(new LayoutTransition());
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        // If the indicator type is not fixed
        if (mIndicatorType != 0) {
            ViewGroup.LayoutParams params = this.getLayoutParams();
            params.height = mDotDiameter;
            requestLayout();
        }
    }

    void updateDot(int length) {
        LENGTH=length;
        if (mIndicatorType == 0) {
            if (length > 0) {
                if (length > mPreviousLength) {
                    fillDot(getChildAt(length - 1));
                } else {
                    emptyDot(getChildAt(length));
                }
                mPreviousLength = length;
            } else {
                // When {@code mPinLength} is 0, we need to reset all the views back to empty
                for (int i = 0; i < getChildCount(); i++) {
                    View v = getChildAt(i);
                    emptyDot(v);
                }
                mPreviousLength = 0;
            }
        } else {
            if (length > 0) {

                if (clcikedDelete == 1) {

                    removeViewAt(length - 1);
                    View dot = new View(getContext());
                       emptyDot(dot);
                    LayoutParams params = new LayoutParams(mDotDiameter,
                                mDotDiameter);
                        params.setMargins(mDotSpacing, 0, mDotSpacing, 0);
                        dot.setLayoutParams(params);
                   addView(dot,length);

                } else {
//                    if (length > mPreviousLength) {
                        View dot = new View(getContext());
                        fillDot(dot);
                        LayoutParams params = new LayoutParams(mDotDiameter,
                                mDotDiameter);
                        params.setMargins(mDotSpacing, 0, mDotSpacing, 0);
                        dot.setLayoutParams(params);

                        addView(dot, LENGTH - 1);
                        removeViewAt(LENGTH);


//                    } else {
//                        removeViewAt(length);
//                    }
                    mPreviousLength = length;
                } }else{
                if(clcikedDelete==0){

                    removeAllViews();
                    mPreviousLength = 0;
                }

                else {
                    removeAllViews();
                    initView(getContext());
                }
                }
                }








//                    if (length > mPreviousLength) {
//                        View dot = new View(getContext());
//                        fillDot(dot);
//                        LayoutParams params = new LayoutParams(mDotDiameter,
//                                mDotDiameter);
//                        params.setMargins(mDotSpacing, 0, mDotSpacing, 0);
//                        dot.setLayoutParams(params);
//
//                        addView(dot, LENGTH - 1);
//                        removeViewAt(LENGTH);
//
//
//                    } else {
//                        removeViewAt(length);
//                    }
//                    mPreviousLength = length;
//                } else{
//                    removeAllViews();
//                    mPreviousLength = 0;
//                }

     //  }
    }

    private void emptyDot(View dot) {
        dot.setBackgroundResource(mEmptyDrawable);
    }

    private void fillDot(View dot) {
        dot.setBackgroundResource(R.drawable.circle);
    }

    public int getPinLength() {
        return mPinLength;
    }

    public void setPinLength(int pinLength) {
        this.mPinLength = pinLength;
        removeAllViews();
        initView(getContext());
    }

    public
    @IndicatorType
    int getIndicatorType() {
        return mIndicatorType;
    }

    public void setIndicatorType(@IndicatorType int type) {
        this.mIndicatorType = type;
        removeAllViews();
        initView(getContext());
    }




}
