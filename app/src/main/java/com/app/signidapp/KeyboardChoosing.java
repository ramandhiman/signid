package com.app.signidapp;

import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.app.signidapp.session.SignIDAppSession;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KeyboardChoosing extends AppCompatActivity {

    @BindView(R.id.keyboard_choose_btn)
    Button keyboard_choose_btn;

    SignIDAppSession session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyboard_listing);
        ButterKnife.bind(this);

        session=new SignIDAppSession(this);
        keyboard_choose_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                imeManager.showInputMethodPicker();

            }
        });
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);


        if(hasFocus) {

            new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity

                    // startActivity(new Intent(SplashScreen.this, MainActivity.class));
                    System.out.println("CURRENT KEYBOARD=========" + Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD));

                    if (Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD).equals("com.app.signidapp/.utils.SoftKeyboard")) {

                        if(session.getKeyShowGoFirstTime().equals("no"))
                        {
                            startActivity(new Intent(KeyboardChoosing.this, SettingActivity.class));
                            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                            finish();
                        }
                        else
                        { startActivity(new Intent(KeyboardChoosing.this, GoActivity.class));
                            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                            finish();}



                    } else {

                       // keyBoardStatusText.setText("SignID Keyboard is not choosen yet");
                    }

                    // close this activity
                    // finish();
                }
            }, 500);



           /* if (Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD).equals("com.app.signid/.utils.SoftKeyboard")) {
                keyBoardStatusText.setText("SignID Keyboard is up and running");
            } else {
                keyBoardStatusText.setText("SignID Keyboard is not choosen yet");
            }*/

            System.out.println("M FOCUSED======" + hasFocus);
        }

    }
}
