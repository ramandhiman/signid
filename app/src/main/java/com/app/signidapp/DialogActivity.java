package com.app.signidapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.app.signidapp.session.SignIDAppSession;

public class DialogActivity extends AppCompatActivity {
    public static boolean iAmOpened = false;

    SignIDAppSession signIDAppSession;
    TextView tvcancel, tvEnter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_fingerprints);
        iAmOpened=true;
        registerReceiver(closeMe, new IntentFilter("closeMe"));
        tvcancel = findViewById(R.id.tvCancel);
        tvEnter = findViewById(R.id.tvEnterPasscode);
        signIDAppSession = new SignIDAppSession(this);
        tvcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if(signIDAppSession!=null) {

            if (signIDAppSession.getPasscode().equals("")) {
                tvEnter.setText("Enable Passcode");
            }


            else {
                tvEnter.setText("Passcode");
            }
        }
        tvEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(signIDAppSession!=null){
                if (signIDAppSession.getPasscode().equals("")) {
                    Intent intent=new Intent(DialogActivity.this,SettingActivity.class);
                    finish();
                    startActivity(intent);

                } else {
                    Intent i = new Intent(getApplicationContext(), SampleActivity.class);

                    i.putExtra("passcode", "alreadysetted");
                    i.putExtra("comingfromservice",true);
                 i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    finish();
                  startActivity(i);


                }}






            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
iAmOpened=false;
Intent i=new Intent("stopProgressBar");
sendBroadcast(i);

    }

    @Override
    public void finish() {
        super.finish();
        iAmOpened=false;
        unregisterReceiver(closeMe);
    }
    BroadcastReceiver closeMe = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
            sendBroadcast(new Intent("FinePayed"));


        }
    };

}
