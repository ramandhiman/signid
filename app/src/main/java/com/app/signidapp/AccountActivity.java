package com.app.signidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.app.signidapp.session.SignIDAppSession;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AccountActivity extends AppCompatActivity {

    @BindView(R.id.backBtn)
    ImageView backBtn;

    @BindView(R.id.text_user_name)
    TextView text_user_name;

    @BindView(R.id.LL_sign_up_container)
    LinearLayout LL_sign_up_container;

    @BindView(R.id.imageViewAc)
    CircleImageView imageViewAc;

    @BindView(R.id.textLoginLogoutStatus)
            TextView textLoginLogoutStatus;

    SignIDAppSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        ButterKnife.bind(this);

        session=new SignIDAppSession(this);


      //  System.out.println("OUTPUT======USER=="+session.getUser_name());



        checkLoginStatus();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        LL_sign_up_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

               if( !session.getUser_name().equals(""))
               {
                   startActivity(new Intent(AccountActivity.this, MainActivity.class).putExtra("from","ac").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                   overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

                   session.setUser_email("");
                   session.setKeyUserImageUrl("");
                   session.setUser_name("");
                   finish();



               }
               else
               {
                   startActivity(new Intent(AccountActivity.this, MainActivity.class).putExtra("from","ac").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                   overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                   finish();
               }




            }
        });

        System.out.println("Image URL====="+session.getKeyUserImageUrl());
        if(!session.getKeyUserImageUrl().isEmpty())
        Picasso.with(this).load(session.getKeyUserImageUrl()).into(imageViewAc);
    }

    private void checkLoginStatus()
    {
        if(!session.getUser_name().equals(""))
        {
            textLoginLogoutStatus.setText("Log out");
            text_user_name.setText(session.getUser_name());
        }
        else
        {
            text_user_name.setText("Anonymous");
        }

    }
    public void onBackPressed() {
        super.onBackPressed();
        SettingActivity.pressedbutton=false;
    }

}
