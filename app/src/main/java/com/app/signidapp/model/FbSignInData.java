package com.app.signidapp.model;

/**
 * Created by RITUPARNA on 6/12/2018.
 */

public class FbSignInData
{
    private String username;

    private String email;

    private String user_id;

    private String google_plus_id;

    private String facebook_id;

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getGoogle_plus_id ()
    {
        return google_plus_id;
    }

    public void setGoogle_plus_id (String google_plus_id)
    {
        this.google_plus_id = google_plus_id;
    }

    public String getFacebook_id ()
    {
        return facebook_id;
    }

    public void setFacebook_id (String facebook_id)
    {
        this.facebook_id = facebook_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [username = "+username+", email = "+email+", user_id = "+user_id+", google_plus_id = "+google_plus_id+", facebook_id = "+facebook_id+"]";
    }
}
