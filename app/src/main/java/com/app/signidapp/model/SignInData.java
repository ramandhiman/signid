package com.app.signidapp.model;

/**
 * Created by RITUPARNA on 6/11/2018.
 */

public class SignInData
{
    private String username;

    private String email;

    private String user_id;

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [username = "+username+", email = "+email+", user_id = "+user_id+"]";
    }
}
