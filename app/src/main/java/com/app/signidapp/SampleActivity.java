package com.app.signidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.app.signidapp.session.SignIDAppSession;
import com.app.signidapp.utils.Constants;

import pinlockview.IndicatorDots;
import pinlockview.PinLockListener;
import pinlockview.PinLockView;

public class SampleActivity extends AppCompatActivity {
    public static final String TAG = "PinLockView";
    public static String packageName = "";
    public static boolean fall = false;
    int i;
    SignIDAppSession signIDAppSession;
    String enteredPin = "";
    String createdPin = "";
    String newCreatedPin = "";
    String newConfirmedPin = "";
    String confirmedPin;
    boolean comingfromservice = false;
    TextView tvProfileName;
    String passcodeSettedOut = "";
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            if (tvProfileName.getText().equals("Confirm your Passcode")) {
//
//                if (!createdPin.equals("")) {
//                    confirmedPin = pin;
//                    if (confirmedPin.equals(createdPin)) {
//
//                        Toast.makeText(SampleActivity.this, "Password set out successfully", Toast.LENGTH_SHORT).show();
//                        signIDAppSession.setPasscode(createdPin);
//
//                        finish();
//
//                    } else {
//
//                        Toast.makeText(SampleActivity.this, "Make sure confirmed password set out matches the created one", Toast.LENGTH_SHORT).show();
//                        tvProfileName.setText("Create Passcode for SignId");
//                        mPinLockView.resetPinLockView();
//
//                    }
//                }

            } else if (tvProfileName.getText().equals(getString(R.string.enterpasscodeforsignId))) {
                enteredPin = pin;


                if (enteredPin.equals(signIDAppSession.getPasscode())) {
                    if (comingfromservice) {

                        Intent i = new Intent(getString(R.string.finePayed));
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        sendBroadcast(i);

                        finishAffinity();


                    } else {

                        Intent i = new Intent(getString(R.string.finePayed));
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                        sendBroadcast(i);

                    }

                } else {
                    Toast.makeText(SampleActivity.this, getString(R.string.enterPasscodeCorrectly), Toast.LENGTH_SHORT).show();
                    mPinLockView.resetPinLockView();
                    mIndicatorDots.initView(SampleActivity.this);

                }
            } else if (tvProfileName.getText().equals(getString(R.string.confirmPasscodeLock))) {
                newConfirmedPin = pin;
                if (newCreatedPin.equals(newConfirmedPin)) {

                    Toast.makeText(SampleActivity.this, getString(R.string.passwordsetted), Toast.LENGTH_SHORT).show();
                    finish();
                    signIDAppSession.setPasscode(newCreatedPin);
                    signIDAppSession.setChangedPasscode(newCreatedPin);
                } else {

                    Toast.makeText(SampleActivity.this, getString(R.string.passcodematchLock), Toast.LENGTH_SHORT).show();
                    tvProfileName.setText(getString(R.string.enterNewPasscodeforLock));
                    mPinLockView.resetPinLockView();

                }


            } else if (tvProfileName.getText().equals(getString(R.string.enterNewPasscodeforLock))) {
//
//
//                newCreatedPin = pin;
//                tvProfileName.setText("Confirm your new Passcode");
//                mPinLockView.resetPinLockView();
//
//            } else {
//
//                Log.d(TAG, "Pin complete: " + pin);
//                createdPin = pin;
//                tvProfileName.setText("Confirm your Passcode");
//                mPinLockView.resetPinLockView();
//            }

            }
        }

        @Override
        public void onEmpty() {

        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sample);


        signIDAppSession = new SignIDAppSession(this);
        tvProfileName = (TextView) findViewById(R.id.profile_name);
        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);

        if (getIntent().getStringExtra(Constants.PASSCODE).equals("alreadysetted")) {

            passcodeSettedOut = getIntent().getStringExtra(Constants.PASSCODE);
            comingfromservice = getIntent().getBooleanExtra(Constants.COMINGFROMSERVICE, false);
            tvProfileName.setText(getString(R.string.enterpasscodeforsignId));

        } else if (getIntent().getStringExtra(Constants.PASSCODE).equals("newPassword")) {

//
//            passcodeSettedOut = getIntent().getStringExtra(Constants.PASSCODE);
//            tvProfileName.setText("Enter new Passcode for SignID");


        }


        mPinLockView.attachIndicatorDots(mIndicatorDots);
        mPinLockView.setPinLockListener(mPinLockListener);


        mPinLockView.setPinLength(4);
        i = mPinLockView.getPinLength();
        mIndicatorDots.initView(SampleActivity.this);


        mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);

    }

    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(getString(R.string.backPressed));
        sendBroadcast(i);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();




    }


    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onStart() {
        super.onStart();

    }
}
