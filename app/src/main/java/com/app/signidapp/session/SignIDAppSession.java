package com.app.signidapp.session;

import android.content.Context;
import android.content.SharedPreferences;


public class SignIDAppSession {
    public static final String KEY_USER_EMAIL = "user_email";
    private static final String PREF_NAME = "SignId_pref";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_NAME = "user_name";
    private static final String KEY_USER_IMAGE_URL = "image_url";
    private static final String KEY_SHOW_SETTING = "show_setting";
    private static final String KEY_SHOW_FIRST_TIME = "first_time";
    private static final String KEY_SHOW_GO_FIRST_TIME = "go_first_time";
    private static final String KEY_USER_PASSCODE = "passcode";
    SharedPreferences signIdPreference;
    SharedPreferences.Editor editor;
    int PRIVATE_MODE = 0;
    private Context context;

    public SignIDAppSession(Context context) {
        this.context = context;
        this.signIdPreference = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.editor = this.signIdPreference.edit();

    }

    public String getPackage() {
        return signIdPreference.getString("package","");
    }

    public void setpackage(String sessionId) {
        editor.putString("package", sessionId);
        //mEditor.apply();
        editor.commit();
    }

    public boolean getFingerPrint() {
        return signIdPreference.getBoolean("fingerPrint", true);
    }

    public void setGetFingerPrint(boolean sessionId) {
        editor.putBoolean("fingerPrint", sessionId);
        //mEditor.apply();
        editor.commit();
    }


    public String getChangedPasscode() {
        return signIdPreference.getString("passcode", "");
    }

    public void setChangedPasscode(String sessionId) {
        editor.putString("passcode", sessionId);
        //mEditor.apply();
        editor.commit();
    }

//

    public String getPasscode() {
        return signIdPreference.getString("passcode", "");
    }

    public void setPasscode(String sessionId) {
        editor.putString("passcode", sessionId);
        //mEditor.apply();
        editor.commit();
    }

    public String getKeyShowGoFirstTime() {
        return signIdPreference.getString(KEY_SHOW_GO_FIRST_TIME, "");
    }

    public void sethowGoFirstTime(String go_first_time) {
        this.editor.putString(KEY_SHOW_GO_FIRST_TIME, go_first_time);
        this.editor.commit();
    }

    public String getKeyShowFirstTime() {
        return signIdPreference.getString(KEY_SHOW_FIRST_TIME, "");
    }

    public void setKeyShowFirstTime(String firstTime) {
        this.editor.putString(KEY_SHOW_FIRST_TIME, firstTime);
        this.editor.commit();
    }

    public String getKeyShowSetting() {

        return signIdPreference.getString(KEY_SHOW_SETTING, "");
    }

    public void setKeySetting(String setting) {
        this.editor.putString(KEY_SHOW_SETTING, setting);
        this.editor.commit();

    }

    public String getKeyUserImageUrl() {
        return signIdPreference.getString(KEY_USER_IMAGE_URL, "");
    }

    public void setKeyUserImageUrl(String image_url) {
        this.editor.putString(KEY_USER_IMAGE_URL, image_url);
        this.editor.commit();
    }

    public String getUser_email() {
        return signIdPreference.getString(KEY_USER_EMAIL, "");
    }

    public void setUser_email(String user_email) {

        this.editor.putString(KEY_USER_EMAIL, user_email);
        this.editor.commit();

    }

    public String getUser_id() {
        return signIdPreference.getString(KEY_USER_ID, "");
    }

    public void setUser_id(String user_id) {
        this.editor.putString(KEY_USER_ID, user_id);
        this.editor.commit();
    }

    public String getUser_name() {
        return signIdPreference.getString(KEY_USER_NAME, "");
    }

    public void setUser_name(String user_name) {
        this.editor.putString(KEY_USER_NAME, user_name);
        this.editor.commit();
    }

    public void user_logout() {
        editor.clear();
        editor.commit();
    }
}
