package com.app.signidapp;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.signidapp.session.SignIDAppSession;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingActivity extends AppCompatActivity {

    public static boolean pressedbutton = false;
    SignIDAppSession signIDAppSession;
    @BindView(R.id.LL_keybord_status_container)
    LinearLayout LL_keybord_status_container;
    @BindView(R.id.LL_lockSet)
    LinearLayout LL_lockSet;
    @BindView(R.id.LL_changePasscode)
    LinearLayout LL_changePasscode;
    @BindView(R.id.LL_signature_setting)
    LinearLayout LL_signature_setting;
    @BindView(R.id.LL_signature_account)
    LinearLayout LL_signature_account;
    @BindView(R.id.LL_feedback)
    LinearLayout LL_feedback;
    @BindView(R.id.LL_support)
    LinearLayout LL_support;
    @BindView(R.id.LL_about)
    LinearLayout LL_about;
    @BindView(R.id.backBtn)
    ImageView backBtn;
    @BindView(R.id.keyBoardStatusText)
    TextView keyBoardStatusText;
    TextView tvPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        tvPassword = findViewById(R.id.tvPasscode);
        signIDAppSession = new SignIDAppSession(this);

        ButterKnife.bind(this);

        if (signIDAppSession != null)

        {
            if (!signIDAppSession.getPasscode().equals("")) {
                LL_changePasscode.setVisibility(View.VISIBLE);
                LL_lockSet.setVisibility(View.GONE);
                tvPassword.setText("Passcode");
            } else {
                LL_lockSet.setVisibility(View.VISIBLE);
                LL_changePasscode.setVisibility(View.GONE);
            }
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        LL_keybord_status_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                imeManager.showInputMethodPicker();


            }
        });


        LL_changePasscode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //trying bro
                pressedbutton = true;
                Intent i = new Intent(SettingActivity.this, LockActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                if (signIDAppSession.getPasscode().equals("")) {
//                    Toast.makeText(SettingActivity.this,"Please set the password first",Toast.LENGTH_SHORT).show();
//                } else {
//                    i.putExtra("passcode", "newPassword");
//                }
                startActivityForResult(i, 2);
            }
        });
        LL_signature_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                imeManager.showInputMethodPicker();*/


                startActivity(new Intent(SettingActivity.this, KeyboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

            }
        });
        LL_lockSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                pressedbutton = true;
//trying bro
                Intent i = new Intent(SettingActivity.this, LockActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (signIDAppSession.getPasscode().equals("")) {
                    i.putExtra("passcode", "");
                } else {
                    i.putExtra("passcode", "alreadysetted");
                }
                startActivityForResult(i, 3);

            }
        });
        LL_signature_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressedbutton = true;
                startActivity(new Intent(SettingActivity.this, AccountActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                //finish();

            }
        });
        LL_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressedbutton = true;
                startActivity(new Intent(SettingActivity.this, FeedbackActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });

        LL_support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://signid.com/support"));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                startActivity(intent);
            }
        });

        LL_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressedbutton = true;
                startActivity(new Intent(SettingActivity.this, AboutActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);


            }
        });


        if (!Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD).equals("com.app.signidapp/.utils.SoftKeyboard")) {
            startActivity(new Intent(SettingActivity.this, KeyboardChoosing.class));
            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            finish();
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        if (signIDAppSession != null)

        {
            if (!signIDAppSession.getPasscode().equals("")) {
                LL_changePasscode.setVisibility(View.VISIBLE);
                LL_lockSet.setVisibility(View.GONE);
                tvPassword.setText("Passcode");
            } else {
                LL_lockSet.setVisibility(View.VISIBLE);
                LL_changePasscode.setVisibility(View.GONE);
            }
        }
        // System.out.println("HELLO========="+ Settings.Secure.getString(getContentResolver(),Settings.Secure.DEFAULT_INPUT_METHOD));

        if (Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD).equals("com.app.signidapp/.utils.SoftKeyboard")) {
            keyBoardStatusText.setText("SignID Keyboard is up and running");
        } else {
            startActivity(new Intent(SettingActivity.this, KeyboardChoosing.class));
            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            finish();

            //keyBoardStatusText.setText("SignID Keyboard is not choosen yet");
        }

        // System.out.println("HELLO========="+ Settings.Secure.getString(getContentResolver(),Settings.Secure.DEFAULT_INPUT_METHOD));

        // put your code here...

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);


        if (hasFocus) {

            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity

                    // startActivity(new Intent(SplashScreen.this, MainActivity.class));
                    System.out.println("CURRENT KEYBOARD=========" + Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD));

                    if (Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD).equals("com.app.signidapp/.utils.SoftKeyboard")) {
                        keyBoardStatusText.setText("SignID Keyboard is up and running");
                    } else {
                        startActivity(new Intent(SettingActivity.this, KeyboardChoosing.class));
                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                        finish();
                    }

                    // close this activity
                    // finish();
                }
            }, 500);



           /* if (Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD).equals("com.app.signid/.utils.SoftKeyboard")) {
                keyBoardStatusText.setText("SignID Keyboard is up and running");
            } else {
                keyBoardStatusText.setText("SignID Keyboard is not choosen yet");
            }*/

            System.out.println("M FOCUSED======" + hasFocus);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!signIDAppSession.getPasscode().equals("")) {
            LL_lockSet.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
//        if (pressedbutton) {
//            ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//            if (am != null) {
//                List<ActivityManager.AppTask> tasks = am.getAppTasks();
//                if (tasks != null && tasks.size() > 0) {
//                    tasks.get(0).setExcludeFromRecents(true);
//                    finish();
//                }
//            }
//        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (am != null) {
            List<ActivityManager.AppTask> tasks = am.getAppTasks();
            if (tasks != null && tasks.size() > 0) {
                tasks.get(0).setExcludeFromRecents(true);
                finish();
            }
        }
    }
}
