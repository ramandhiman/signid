package com.app.signidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.signidapp.model.SignInResponse;
import com.app.signidapp.utils.CommonLoadingDialog;
import com.app.signidapp.utils.CommonValidationDialog;
import com.app.signidapp.utils.Content;
import com.app.signidapp.utils.EmailValidator;
import com.google.gson.Gson;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedbackActivity extends AppCompatActivity {

    @BindView(R.id.backBtn)
    ImageView backBtn;

    @BindView(R.id.feedbackSendText)
    TextView feedbackSendText;

    @BindView(R.id.inputFeedBack)
    EditText inputFeedBack;

    @BindView(R.id.inputEmail)
    EditText inputEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        feedbackSendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validateEmailEmpty() && validateEmail() && validateFeedbackText())
                {
                    httpfeedBackSend();
                }

            }
        });



        inputFeedBack.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                    String feedbackText=s.toString();
                    if(feedbackText.length()>0)
                    {
                        feedbackSendText.setTextColor(getColor(R.color.colorWhite));
                        feedbackSendText.setEnabled(true);
                    }
                    else
                    {
                        feedbackSendText.setTextColor(getColor(R.color.colorGrey2));
                        feedbackSendText.setEnabled(false);
                    }
            }
        });
    }

    private void httpfeedBackSend()
    {
        try
        {
            String url = "http://www.signidapp.com/SignId/Customers_Feedback.php?email="+inputEmail.getText().toString().trim()+"&message="+inputFeedBack.getText().toString()+"";

            System.out.println("URL==="+url);
            url = url.replace(" ", "%20");

            CommonLoadingDialog.showLoadingDialog(FeedbackActivity.this,"Wait..");



            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            CommonLoadingDialog.closeLoadingDialog();


                            System.out.println("RESPONSE====="+response);



                            try
                            {
                                JSONObject jsonObject=new JSONObject(response);
                                String status= jsonObject.getString("status");
                                String message=jsonObject.getString("message");

                                if(status.equals("1"))
                                {
                                    Toast.makeText(FeedbackActivity.this, message, Toast.LENGTH_SHORT).show();
                                    inputEmail.setText("");
                                    inputFeedBack.setText("");
                                }
                                else
                                {
                                    Toast.makeText(FeedbackActivity.this, message, Toast.LENGTH_SHORT).show();
                                }


                            }
                            catch (Exception ex)
                            {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR=="+ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR=="+error.toString());
                        }
                    }) ;

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(FeedbackActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        }
        catch (Exception ex)
        {
            System.out.println("OUTPUT ======ERROR=="+ex.toString());
        }
        System.out.println("M EXECUTED======"+feedbackSendText.getText());
    }



    private boolean validateEmail()
    {
        if (new EmailValidator().validateEmail(inputEmail.getText().toString().trim()))
        {
                return  true;
        }
        else
        {
            CommonValidationDialog.showLoadingDialog(FeedbackActivity.this,"Invalid email format");
              //  Toast.makeText(FeedbackActivity.this,"Invalid email format", Toast.LENGTH_SHORT).show();
                return false;
        }


    }

    private boolean validateEmailEmpty()
    {
        if(inputEmail.getText().toString().trim().equals(""))
        {
            CommonValidationDialog.showLoadingDialog(FeedbackActivity.this,"Email cannot be empty");
            return false;
        }
        return true;
    }



    private boolean validateFeedbackText()
    {


        if (inputFeedBack.getText().toString().trim().equals(""))
        {
            Toast.makeText(FeedbackActivity.this,"Feedback cannot be empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        else
        {
            return true;
        }

    }
    public void onBackPressed() {
        super.onBackPressed();

    }
}
