package com.app.signidapp;

import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.app.signidapp.font.MyriadProTextView;
import com.app.signidapp.reciever.MyAdmin;
import com.app.signidapp.session.SignIDAppSession;
import com.app.signidapp.utils.SoftKeyboard;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.mainViewPager)
    ViewPager mainViewPager;
    @BindView(R.id.viewPagerCountDots)
    LinearLayout viewPagerCountDots;
    @BindView(R.id.viewPagerIndicator)
    RelativeLayout viewPagerIndicator;

    MainPagerAdapter adapter;
    @BindView(R.id.text_get_started)
    MyriadProTextView textGetStarted;

    @BindView(R.id.LL_get_started_container)
    LinearLayout LL_get_started_container;

    /*@BindView(R.id.LL_bottom)
    LinearLayout LL_bottom;*/
    private int dotsCount;

    private ImageView[] dots;
    private int page;
DevicePolicyManager dManager;
    //private Integer[] img_array = {R.drawable.slider_test, R.drawable.slider_test, R.drawable.slider_test, R.drawable.slider_test,R.drawable.slider_test};
    private Integer[] img_array = {R.drawable.slider1, R.drawable.slider2, R.drawable.slider3, R.drawable.slider4, R.drawable.slider5};
    private Handler handler;
    private int delay = 3000;
    Runnable runnable;

    SignIDAppSession session;




    /*=====LOCKING SYSTEM====*/
    static final int RESULT_ENABLE = 1;

    DevicePolicyManager deviceManger;
    ActivityManager activityManager;
    ComponentName compName;

    SoftKeyboard.ScreenOnOffReceiver screenOnOffReceiver;


    /*=======================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Fullscreen responsible code*/
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
*/
        /*getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);*/

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);



        session=new SignIDAppSession(this);




        if(session.getKeyShowFirstTime().equals("yes"))
        {

        }
        else
        {
            //trying
          //  manageDeviceAdministration();
        }

        /*This block of code is responsible for handling adjustment of layout as per soft key*/




        /*if(SignIdSingleton.getInstance().isHasSoftKey())
        {
            RelativeLayout.LayoutParams buttonLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            buttonLayoutParams.setMargins(0,0,0,50);

            buttonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            LL_get_started_container.setLayoutParams(buttonLayoutParams);

        }*/

        handler = new Handler();

        adapter = new MainPagerAdapter(MainActivity.this, img_array);

        mainViewPager.addOnPageChangeListener(new CircularViewPagerHandler(mainViewPager));
        mainViewPager.setAdapter(adapter);
        mainViewPager.setOffscreenPageLimit(5);


        /*mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position)
            {
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(getResources().getDrawable(R.drawable.dot_non_selected));
                }

                System.out.println("PAGE COUNT======="+position);
                page=position;
                dots[position].setImageDrawable(getResources().getDrawable(R.drawable.dot_select));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/
        setUiPageViewController();


        runnable = new Runnable() {
            public void run() {
               // System.out.println("PAGE COUNT======="+adapter.getItemPosition(mainViewPager));
                if (adapter.getCount() == page) {
                    page = 0;
                    dots[0].setImageDrawable(getResources().getDrawable(R.drawable.dot_select));
                    mainViewPager.setCurrentItem(page, false);
                } else {
                    page++;
                    mainViewPager.setCurrentItem(page, false);
                }


               // System.out.println("PAGE NO=====" + page);

                handler.postDelayed(this, delay);
            }
        };

        LL_get_started_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               System.out.println("OUTPUT===="+session.getKeyShowFirstTime());

                if( session.getKeyShowFirstTime().equals(""))
                {
                    session.setKeyShowFirstTime("yes");
                    startActivity(new Intent(MainActivity.this, ContinueActivity.class));
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    finish();
                }
                else
                {
                    session.setKeyShowFirstTime("yes");
                    startActivity(new Intent(MainActivity.this, ContinueActivity.class));
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    finish();
                }



            }










        });
    }

    private void manageDeviceAdministration()
    {

//
//        deviceManger = (DevicePolicyManager) getSystemService(
//                Context.DEVICE_POLICY_SERVICE);
//        activityManager = (ActivityManager) getSystemService(
//                Context.ACTIVITY_SERVICE);
        compName = new ComponentName(getApplicationContext(), MyAdmin.class);

        Intent intent = new Intent(DevicePolicyManager
                .ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                compName);
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                "Additional text explaining why this needs to be added.");
        startActivityForResult(intent, RESULT_ENABLE);
        //compName = new ComponentName("com.android.settings", "com.android.settings.DeviceAdminSettings");


    }


    public class MainPagerAdapter extends PagerAdapter {
        Context context;
        Integer[] img;
        LayoutInflater layoutInflater;


        public MainPagerAdapter(Context context, Integer[] img) {
            this.context = context;
            this.img = img;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return img.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            final View itemView = layoutInflater.inflate(R.layout.item_pager, container, false);

            final ImageView imageView = (ImageView) itemView.findViewById(R.id.m_image);


            int index = position %img.length;


            imageView.setImageResource(img[index]);


            container.addView(itemView);

            //listening to image click
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*if (position == 0) {
                        startActivity(new Intent(getActivity(), DishdishaStyleActivity.class));
                        productFlag = "1";
                    }
                    else if (position == 1) {
                        TefalApp.getInstance().setToolbar_title("DARAA STORES");
                        startActivity(new Intent(getActivity(), OtherStoresActivity.class).putExtra("flag", "Daraa"));
                        productFlag = "3";
                    }
                    else if (position == 2) {
                        TefalApp.getInstance().setToolbar_title("ABAYA STORES");
                        startActivity(new Intent(getActivity(), OtherStoresActivity.class).putExtra("flag", "Abaya"));
                        productFlag = "2";
                    }
                    else if (position == 3) {
                        startActivity(new Intent(getActivity(), AccessoriesActivity.class));
                        productFlag = "4";
                    }*/
                }
            });

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }


    private void setUiPageViewController() {

        dotsCount = adapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(MainActivity.this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.dot_non_selected));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(8, 0, 8, 0);

            viewPagerCountDots.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.dot_select));
    }


    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runnable, delay);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }


    class CircularViewPagerHandler implements ViewPager.OnPageChangeListener {
        private ViewPager mViewPager;
        private int mCurrentPosition;
        private int mScrollState;

        public CircularViewPagerHandler(final ViewPager viewPager) {
            mViewPager = viewPager;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(final int position) {

            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.dot_non_selected));
            }

            page=position;
            dots[position].setImageDrawable(getResources().getDrawable(R.drawable.dot_select));
            mCurrentPosition = position;
        }

        @Override
        public void onPageScrollStateChanged(final int state) {
            handleScrollState(state);
            mScrollState = state;
        }

        private void handleScrollState(final int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
                setNextItemIfNeeded();
            }
        }

        private void setNextItemIfNeeded() {
            if (!isScrollStateSettling()) {
                handleSetNextItem();
            }
        }

        private boolean isScrollStateSettling() {
            return mScrollState == ViewPager.SCROLL_STATE_SETTLING;
        }

        private void handleSetNextItem() {
            final int lastPosition = mViewPager.getAdapter().getCount() - 1;
            if (mCurrentPosition == 0) {
                mViewPager.setCurrentItem(lastPosition, false);
            } else if (mCurrentPosition == lastPosition) {
                mViewPager.setCurrentItem(0, false);
            }

            System.out.println("PAGE POGITION===="+mCurrentPosition);
        }
    }
}
