package com.app.signidapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.signidapp.font.GothamMediumButton;
import com.app.signidapp.model.SignInResponse;
import com.app.signidapp.session.SignIDAppSession;
import com.app.signidapp.utils.CommonLoadingDialog;
import com.app.signidapp.utils.CommonValidationDialog;
import com.app.signidapp.utils.Content;
import com.app.signidapp.utils.EmailValidator;
import com.google.gson.Gson;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignInActivity extends AppCompatActivity {

    @BindView(R.id.LL_bottom_container)
    LinearLayout LL_bottom_container;
    @BindView(R.id.LL_logo_container)
    LinearLayout LLLogoContainer;
    @BindView(R.id.inputEmail)
    EditText inputEmail;
    @BindView(R.id.LL_email_container)
    LinearLayout LLEmailContainer;
    @BindView(R.id.inputPass)
    EditText inputPass;
    @BindView(R.id.LL_password_container)
    LinearLayout LLPasswordContainer;
    @BindView(R.id.LL_login_container)
    LinearLayout LLLoginContainer;
    @BindView(R.id.LL_forgotPasswordContainer)
    LinearLayout LLForgotPasswordContainer;
    @BindView(R.id.sign_in_btn)
    GothamMediumButton signInBtn;
    @BindView(R.id.LL_sign_in_button_container)
    LinearLayout LLSignInButtonContainer;
    @BindView(R.id.LL_signUp)
    LinearLayout LL_signUp;

    @BindView(R.id.textForgotPassword)
    TextView textForgotPassword;

    @BindView(R.id.backBtn)
    ImageView backBtn;

    SignIDAppSession session;


    private AlertDialog.Builder alertDialogBuilder2;
    private AlertDialog alertDialog;
    private AlertDialog.Builder alertDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        session = new SignIDAppSession(this);


        LL_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignInActivity.this, SignUpActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

            }
        });

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateEmptyEmail() && validateEmail() && validatePassword()) {
                    httpLogin();
                }

            }
        });


        textForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        init();
    }


    private void init() {
        alertDialogBuilder = new AlertDialog.Builder(this);
    }


    private void showDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.forgot_password_dialog_layout, null);

        LinearLayout LL_continue_container = (LinearLayout) dialogView.findViewById(R.id.LL_continue_container);
        LinearLayout LL_cancel_container = (LinearLayout) dialogView.findViewById(R.id.LL_cancel_container);
        final EditText input_email = (EditText) dialogView.findViewById(R.id.input_email);



        /*TextView text1=(TextView)dialogView.findViewById(R.id.text1);
        TextView text2=(TextView)dialogView.findViewById(R.id.text2);
        TextView text3=(TextView)dialogView.findViewById(R.id.text3);

        if(tag.equals("fb"))
        {
            text1.setText(getResources().getString(R.string.text1_fb));
            text2.setText(getResources().getString(R.string.text2_fb));
            text3.setText(getResources().getString(R.string.text3_fb));
        }
        if (tag.equals("gl"))
        {
            text1.setText(getResources().getString(R.string.text1_gl));
            text2.setText(getResources().getString(R.string.text2_gl));
            text3.setText(getResources().getString(R.string.text3_gl));
        }*/


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        LL_continue_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateForgetEmail(input_email.getText().toString().trim())) {
                    alertDialog.dismiss();
                    httpForgotPassword(input_email.getText().toString().trim());
                }


                //Toast.makeText(SignInActivity.this, "Hi", Toast.LENGTH_SHORT).show();
            }
        });

        LL_cancel_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                // Toast.makeText(SignInActivity.this, "Hello", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void httpForgotPassword(String email) {
        try {
            String url = Content.baseURL + "Forgot_Password.php?email=" + email + "";

            System.out.println("URL===" + url);


            CommonLoadingDialog.showLoadingDialog(SignInActivity.this, "Checking..");


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();


                            System.out.println("RESPONSE=====" + response);


                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");

                                if (status.equals("1")) {
                                    Toast.makeText(SignInActivity.this, message, Toast.LENGTH_SHORT).show();

                                } else {
                                    Toast.makeText(SignInActivity.this, message, Toast.LENGTH_SHORT).show();
                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SignInActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex.toString());
        }
    }

    private void httpLogin() {
        try {
            String url = Content.baseURL + "Login.php?user_input=" + inputEmail.getText().toString().trim() + "&password=" + inputPass.getText().toString() + "";

            System.out.println("URL===" + url);


            CommonLoadingDialog.showLoadingDialog(SignInActivity.this, "Log In..");


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("RESPONSE=====" + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");

                                if (status.equals("1")) {
                                    Gson gson = new Gson();
                                    SignInResponse signInResponse = gson.fromJson(response, SignInResponse.class);
                                    String user_id = signInResponse.getData().getUser_id();
                                    String email_id = signInResponse.getData().getEmail();
                                    String user_name = signInResponse.getData().getUsername();

                                    session.setUser_email(email_id);
                                    session.setUser_id(user_id);
                                    session.setUser_name(user_name);


                                    System.out.println("OUTPUT=============user_id==" + session.getUser_id());
                                    System.out.println("OUTPUT=============email_id==" + session.getUser_email());
                                    System.out.println("OUTPUT=============user_name==" + session.getUser_name());


                                    Toast.makeText(SignInActivity.this, message, Toast.LENGTH_SHORT).show();

                                    // showLoginSuccess("Login successful");

                                    startActivity(new Intent(SignInActivity.this, SettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    finish();

                                } else {
                                    String message1 = jsonObject.getString("message");
                                    if (message1.equals("Username or Password may be wrong ")) {

                                        showLoginFails("Please make sure your entered valid Username or Password ");


                                    } else {
                                        showLoginFails("Login fail!make sure you have verified your credentials first");

                                    }

                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SignInActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex.toString());
        }
    }


    private boolean validateEmail() {
        if (new EmailValidator().validateEmail(inputEmail.getText().toString().trim())) {
            return true;
        } else {
            Toast.makeText(SignInActivity.this, "Invalid email format", Toast.LENGTH_SHORT).show();
            return false;
        }


    }

    private boolean validateEmptyEmail() {
        if (inputEmail.getText().toString().trim().equals("")) {
            CommonValidationDialog.showLoadingDialog(SignInActivity.this, "Please fill the empty field");
            return false;
        } else {

            if (new EmailValidator().validateEmail(inputEmail.getText().toString().trim())) {
                return true;
            } else {
                CommonValidationDialog.showLoadingDialog(SignInActivity.this, "Please enter a valid email");
                return false;
            }

        }


    }

    private boolean validatePassword() {
        if (inputPass.getText().toString().trim().equals("")) {
            CommonValidationDialog.showLoadingDialog(SignInActivity.this, "Please fill the empty field");
            return false;
        } else {
            return true;
        }
    }


    private boolean validateForgetEmail(String email) {
        if (email.trim().equals("")) {
            Toast.makeText(this, "Email should not be empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void showLoginSuccess(String msg) {
        alertDialogBuilder2 = new AlertDialog.Builder(SignInActivity.this);
        LayoutInflater LayoutInflater = getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dailog_validation_layout, null);
        TextView textMessage = (TextView) dialogView.findViewById(R.id.textMessage);
        TextView okBtn = (TextView) dialogView.findViewById(R.id.okBtn);

        textMessage.setText(msg);


        alertDialogBuilder.setView(dialogView);
        alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        alertDialog.show();


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                String pkg = "";

                startActivity(new Intent(SignInActivity.this, SettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();

               /* InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                for (int i=0;i<imeManager.getEnabledInputMethodList().size();i++)
                {
                    pkg=imeManager.getEnabledInputMethodList().get(i).getPackageName();

                    if(pkg.equals("com.app.signidapp"))
                    {
                        break;
                    }
                    System.out.println("PACKAGE==========="+pkg);

                    *//*if( !imeManager.getEnabledInputMethodList().get(i).getPackageName().equals("com.app.signidapp"))
                    {
                        // session.setKeySetting("yes");
                        startActivity(new Intent(ContinueActivity.this, KeyboardSettingActivity.class));
                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                        finish();
                    }*//*
                }

                // System.out.println("PACKAGE==========="+pkg);

                if(pkg.equals("com.app.signidapp"))
                {
                    startActivity(new Intent(SignInActivity.this, SettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                }
                else
                {
                    startActivity(new Intent(SignInActivity.this, KeyboardChoosing.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                }*/


            }
        });
    }

    private void showLoginFails(String msg) {
        alertDialogBuilder2 = new AlertDialog.Builder(SignInActivity.this);
        LayoutInflater LayoutInflater = getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dailog_validation_layout, null);
        TextView textMessage = (TextView) dialogView.findViewById(R.id.textMessage);
        TextView okBtn = (TextView) dialogView.findViewById(R.id.okBtn);

        textMessage.setText(msg);


        alertDialogBuilder.setView(dialogView);
        alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        alertDialog.show();


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                /*String pkg="";

                InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                for (int i=0;i<imeManager.getEnabledInputMethodList().size();i++)
                {
                    pkg=imeManager.getEnabledInputMethodList().get(i).getPackageName();

                    if(pkg.equals("com.app.signidapp"))
                    {
                        break;
                    }
                    System.out.println("PACKAGE==========="+pkg);

                    *//*if( !imeManager.getEnabledInputMethodList().get(i).getPackageName().equals("com.app.signidapp"))
                    {
                        // session.setKeySetting("yes");
                        startActivity(new Intent(ContinueActivity.this, KeyboardSettingActivity.class));
                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                        finish();
                    }*//*
                }

                // System.out.println("PACKAGE==========="+pkg);

                if(pkg.equals("com.app.signidapp"))
                {
                    startActivity(new Intent(SignInActivity.this, SettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                }
                else
                {
                    startActivity(new Intent(SignInActivity.this, KeyboardSettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                }*/


            }
        });
    }

}
