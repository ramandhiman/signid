package com.app.signidapp.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.TextView;
import android.widget.Toast;

import com.app.signidapp.DialogActivity;
import com.app.signidapp.MainActivity;
import com.app.signidapp.R;
import com.app.signidapp.app.SignIdSingleton;
import com.app.signidapp.session.SignIDAppSession;

import static com.app.signidapp.DialogActivity.iAmOpened;


/**
 * Created by whit3hawks on 11/16/16.
 */

public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {
    SignIDAppSession signIDAppSession;
    private Context context;



    // Constructor
    public FingerprintHandler(Context mContext) {
        context = mContext;
        signIDAppSession=new SignIDAppSession(context);
    }

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject)
    {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
       // this.update("Fingerprint Authentication error\n" + errString);
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        //this.update("Fingerprint Authentication help\n" + helpString);
    }

    @Override
    public void onAuthenticationFailed() {
        Intent intent=new Intent("FingerPrintfailed");
        context.sendBroadcast(intent);
        Toast.makeText(context, "FingerPrintfailed", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

if(iAmOpened){


    context.sendBroadcast(new Intent("closeMe"));

}
else {context.sendBroadcast(new Intent("FinePayed"));

}
       /* context.finish();*/
        //Toast.makeText(context, "SUCCESS", Toast.LENGTH_SHORT).show();

        //context.unregisterReceiver(fingerPrintReciever);



        /*context.unregisterReceiver(fingerPrintReciever);
        context.finish();*/

       /* ((Activity) context).finish();
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);*/
    }

    private void update(String e){

        try {
            TextView textView = (TextView) ((Activity) context).findViewById(R.id.errorText);
            textView.setText(e);
        }
        catch (Exception ex)
        {

        }
    }

}
