package com.app.signidapp.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.app.signidapp.R;


/**
 * Created by RITUPARNA on 5/15/2018.
 */

/**
 * Created by RITUPARNA on 5/15/2018.
 */






public class CommonLoadingDialog
{
    private static AlertDialog.Builder alertDialogBuilder;
    private static AlertDialog alertDialog;

    public static void showLoadingDialog(Activity activity, String msg)
    {
        alertDialogBuilder= new AlertDialog.Builder(activity);
        LayoutInflater LayoutInflater = activity.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_loading_layout, null);
        TextView messageText=(TextView)dialogView.findViewById(R.id.textMessage);
        messageText.setText(msg);


        alertDialogBuilder.setView(dialogView);
        alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL| Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public static void closeLoadingDialog()
    {
        alertDialog.dismiss();
    }
}
