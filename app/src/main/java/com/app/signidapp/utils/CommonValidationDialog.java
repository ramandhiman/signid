package com.app.signidapp.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.app.signidapp.R;

public class CommonValidationDialog
{
    private static AlertDialog.Builder alertDialogBuilder;
    private static AlertDialog alertDialog;

    public static void showLoadingDialog(Activity activity, String msg)
    {
        alertDialogBuilder= new AlertDialog.Builder(activity);
        LayoutInflater LayoutInflater = activity.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dailog_validation_layout, null);
        LinearLayout LL_ok=(LinearLayout)dialogView.findViewById(R.id.LL_ok);
        TextView textMessage=(TextView)dialogView.findViewById(R.id.textMessage);
        TextView okBtn=(TextView)dialogView.findViewById(R.id.okBtn);

        textMessage.setText(msg);


        alertDialogBuilder.setView(dialogView);
        alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL| Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        alertDialog.show();


        LL_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                alertDialog.dismiss();
            }
        });
    }

    public static void closeLoadingDialog()
    {
        alertDialog.dismiss();
    }
}
