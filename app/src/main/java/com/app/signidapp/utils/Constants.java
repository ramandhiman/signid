package com.app.signidapp.utils;

public class Constants {
    public static final String PASSCODE = "passcode";
    public static final String COMINGFROMSERVICE="comingfromservice";
    public static final int THREAD_TIME=2000;
}
