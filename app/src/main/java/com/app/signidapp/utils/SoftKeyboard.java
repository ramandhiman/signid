package com.app.signidapp.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ClipDescription;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.hardware.fingerprint.FingerprintManager;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.provider.Settings;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.v13.view.inputmethod.EditorInfoCompat;
import android.support.v13.view.inputmethod.InputConnectionCompat;
import android.support.v13.view.inputmethod.InputContentInfoCompat;
import android.support.v4.app.ActivityCompat;
import android.text.InputType;
import android.text.method.MetaKeyKeyListener;
import android.util.Log;
import android.view.InflateException;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputBinding;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.signidapp.DialogActivity;
import com.app.signidapp.R;
import com.app.signidapp.SampleActivity;
import com.app.signidapp.SettinActivity;
import com.app.signidapp.SettingActivity;
import com.app.signidapp.app.SignIdApplication;
import com.app.signidapp.session.SignIDAppSession;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;


public class SoftKeyboard extends InputMethodService implements KeyboardView.OnKeyboardActionListener {

    static final boolean PROCESS_HARD_KEYS = true;
    static final int RESULT_ENABLE = 1;
    private static final String TAG = "SoftKeyboard";
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "SIGN ID";
    static boolean arrowPressed = false;
    private static SignIdApplication appInstance;
    private static String pack = "";
    public KeyboardView kv;
    Button btnPassword;

    SignIDAppSession signIDAppSession;

    ProgressBar mProgressbar2;


    boolean loaderOff = false;
    boolean excecute = false;
    LinearLayout view;

    ArrayList<Uri> uris = new ArrayList<>();
    RelativeLayout mInputView2;
    RelativeLayout signature_panel;

    LinearLayout LL_validate;
    LinearLayout LL_keyboard_holder;

    SignatureView signatureView;
    LinearLayout LL_pad_holder;

    ImageView signature_icon;
    TextView validateText;
    LinearLayout LL_keyboard, LL_globe, LL_clear_btn, LL_keyboard2;
    boolean gifSupported = false;

    RelativeLayout LL_temp;
    KeyguardManager keyguardManager;
    FingerprintManager fingerprintManager;
    FingerPrintReciever fingerPrintReciever;

    String messagePackage = "";
    AudioManager am;
    BroadcastReceiver FingerPrintfailed = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mProgressbar2.setVisibility(View.GONE);
            Intent i = new Intent(getApplicationContext(), DialogActivity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

        }
    };
    BroadcastReceiver stopProgressBar = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mProgressbar2.setVisibility(View.GONE);
        }
    };
    BroadcastReceiver packageNameCalculation = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            String packageName = getCurrentInputEditorInfo().packageName;
            pack = packageName;
            SampleActivity.packageName = pack;
        }
    };
    private InputMethodManager mInputMethodManager;
    private LatinKeyboardView latinKeyboardView;
    private CandidateView mCandidateView;
    private CompletionInfo[] mCompletions;
    private StringBuilder mComposing = new StringBuilder();
    private boolean mPredictionOn;
    private boolean mCompletionOn;
    private int mLastDisplayWidth;
    private boolean mCapsLock;
    private long mLastShiftTime;
    private long mMetaState;
    private LatinKeyboard mSymbolsKeyboard;
    private LatinKeyboard mSymbolsShiftedKeyboard;
    private LatinKeyboard mQwertyKeyboard;
    //ScreenOnOffReceiver screenOnOffReceiver;
    private LatinKeyboard mCurKeyboard;
    private String mWordSeparators;
    private Button close_text;
    /*=======================*/
    private Button verify_text;
    private Bitmap bitmap;
    BroadcastReceiver Finepayed = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            validateText.setText(getString(R.string.validating));
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    mProgressbar2.setVisibility(View.GONE);
                    save_and_send_signature();
                    signatureView.clear();
                }
            }, Constants.THREAD_TIME);
        }
    };
    private KeyStore keyStore;
    private Cipher cipher;
    private ProgressBar mProgressBar;

    public static boolean isDeviceLocked(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            KeyguardManager manager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            if (manager.isDeviceLocked())
                return true;
        }
        return false;
    }

    public static boolean isDeviceSecured(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            KeyguardManager manager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            if (manager.isDeviceSecure())
                return true;
        }
        return false;
    }

    /**
     * Main initialization of the input_try method component.  Be sure to call
     * to super class.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("M CALLED FROM onCreate()");
        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        mWordSeparators = getResources().getString(R.string.word_separators);
        signatureView = new SignatureView(this, null);
        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
        am = (AudioManager) getSystemService(AUDIO_SERVICE);
        signatureView.clear();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }


//     regist


    @Override
    public void onStartInputView(EditorInfo info, boolean restarting) {
        String[] mimeTypes = EditorInfoCompat.getContentMimeTypes(info);
        System.out.println("M CALLED FROM onStartInputView");
        System.out.println("HELLO RITU===FROM ON START");
        System.out.println("MIME TYPE LEGNTH======" + mimeTypes.length);
        LL_temp.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);

        gifSupported = false;
        for (String mimeType : mimeTypes) {
            if (ClipDescription.compareMimeTypes(mimeType, "image/gif")) {
                gifSupported = true;
            }
        }
    }

    /**
     * This is the point where you can do all of your UI initialization.  It
     * is called after creation and any configuration change.
     */
    @Override
    public void onInitializeInterface() {

        System.out.println("M CALLED FROM onInitializeInterface");
        if (mQwertyKeyboard != null) {
            // Configuration changes can happen after the keyboard gets recreated,
            // so we need to be able to re-build the keyboards if the available
            // space has changed.
            int displayWidth = getMaxWidth();
            if (displayWidth == mLastDisplayWidth) return;
            mLastDisplayWidth = displayWidth;
        }
        mQwertyKeyboard = new LatinKeyboard(this, R.xml.qwerty);
        mSymbolsKeyboard = new LatinKeyboard(this, R.xml.symbols);
        mSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
//        if (LL_validate != null)
////            LL_validate.setEnabled(true);
    }

    @Override
    public View onCreateInputView() {

        System.out.println("M CALLED FROM onCreateInputView()");

        this.mInputView2 = (RelativeLayout) this.getLayoutInflater().inflate(R.layout.input_try, null);
        signature_icon = (ImageView) this.mInputView2.findViewById(R.id.signature_icon);
        kv = (LatinKeyboardView) mInputView2.findViewById(R.id.keyboard);
        kv.setBackground(getApplicationContext().getResources().getDrawable(R.color.colorText));


        kv.setKeyboard(mQwertyKeyboard);
        kv.setOnKeyboardActionListener(this);
        signIDAppSession = new SignIDAppSession(getApplicationContext());
        //trying
        kv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    kv.closing();
                }
                return false;

            }
        });
        kv.setFocusableInTouchMode(false);
        validateText = (TextView) this.mInputView2.findViewById(R.id.validateText);
        LL_validate = (LinearLayout) this.mInputView2.findViewById(R.id.LL_validate);

        LL_keyboard = (LinearLayout) this.mInputView2.findViewById(R.id.LL_keyboard);

        LL_globe = (LinearLayout) this.mInputView2.findViewById(R.id.LL_globe);

        LL_clear_btn = (LinearLayout) this.mInputView2.findViewById(R.id.LL_clear_btn);

        LL_keyboard2 = (LinearLayout) this.mInputView2.findViewById(R.id.LL_keyboard2);

        signature_panel = (RelativeLayout) this.mInputView2.findViewById(R.id.signature_panel);

        if (!arrowPressed) {
            for (int i = 0; i < kv.getKeyboard().getKeys().size(); i++) {

                if (kv.getKeyboard().getKeys().get(i).modifier) {
                    kv.getKeyboard().getKeys().get(i).icon = getResources().getDrawable(R.drawable.caps_xxhdpi);
                }
            }
        }

        if (signatureView != null) {
            ViewGroup parent = (ViewGroup) signatureView.getParent();
            if (parent != null) {
                parent.removeView(signatureView);
            }
        }
        try {
            signature_panel.addView(signatureView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } catch (InflateException e) {

        }

        LL_temp = (RelativeLayout) this.mInputView2.findViewById(R.id.LL_temp);

        LL_pad_holder = (LinearLayout) this.mInputView2.findViewById(R.id.LL_pad_holder);

        LL_keyboard_holder = (LinearLayout) this.mInputView2.findViewById(R.id.LL_keyboard_holder);

        mProgressBar = (ProgressBar) this.mInputView2.findViewById(R.id.mProgressBar);
        mProgressbar2 = (ProgressBar) this.mInputView2.findViewById(R.id.mProgressBar2);
        LL_keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LL_pad_holder.setVisibility(View.GONE);
                LL_keyboard_holder.setVisibility(View.VISIBLE);
            }
        });
        LL_globe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleLanguageSwitch();
            }
        });

        signature_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        LL_clear_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.onClickUndo();
            }
        });

        LL_clear_btn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                signatureView.clear();
                return true;
            }
        });
        LL_keyboard2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleLanguageSwitch();
            }
        });

        LL_validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signatureView.hasFocus()) {
                    if (signatureView.paths.size() == 0) {
                        Toast.makeText(getApplicationContext(), getString(R.string.requiredActions), Toast.LENGTH_SHORT).show();
                    } else {//Toast.makeText(getApplicationContext(),"Please Wait upto 3 Seconds...",Toast.LENGTH_LONG).show();
                        mProgressbar2.setVisibility(View.VISIBLE);
                        fingerPrintReciever = new FingerPrintReciever();
                        registerReceiver(packageNameCalculation, new IntentFilter(getString(R.string.packageCalculation)));
                        registerReceiver(stopProgressBar, new IntentFilter(getString(R.string.stopProgressBar)));
                        registerReceiver(Finepayed, new IntentFilter(getString(R.string.finePayed)));
                        registerReceiver(FingerPrintfailed, new IntentFilter(getString(R.string.FingerPrintfailed)));
                        IntentFilter intentFilter = new IntentFilter();
                        intentFilter.addAction(getString(R.string.signIdapp));
                        registerReceiver(fingerPrintReciever, intentFilter);

                        fingerPrintActivity();

                    }

                }
            }
        });


        return mInputView2;
    }

    private void fingerPrintActivity() {


        if (!fingerprintManager.isHardwareDetected()) {

            if (!signIDAppSession.getPasscode().equals("")) {

                mProgressbar2.setVisibility(View.GONE);
                passcodeSecurity(getApplicationContext());
            } else {

                mProgressbar2.setVisibility(View.GONE);
                Intent intent = new Intent(getApplicationContext(), SettinActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(getString(R.string.fingerPrint), false);
                startActivity(intent);


            }

        } else {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                //textView.setText("Fingerprint authentication permission not enabled");
            } else {
                // Check whether at least one com.app.signidappfingerprint is registered
                if (!fingerprintManager.hasEnrolledFingerprints()) {

                    mProgressbar2.setVisibility(View.GONE);
                    Intent intent = new Intent(getApplicationContext(), SettinActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);


                } else {// Checks whether lock screen security is enabled or not
                    if (!keyguardManager.isKeyguardSecure()) {

                    } else {
                        LL_temp.setVisibility(View.VISIBLE);
                        btnPassword = LL_temp.findViewById(R.id.btnPasswordwithfingerPrint);
                        loaderOff = true;

                        mProgressbar2.setVisibility(View.GONE);

                        if (!signIDAppSession.getPasscode().equals("")) {

                            btnPassword.setText(getString(R.string.validateUsingPasscode));
                        } else {


                            btnPassword.setText(getString(R.string.enablePasscode));


                        }

                        btnPassword.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mProgressbar2.setVisibility(View.GONE);
                                if (btnPassword.getText().equals(getString(R.string.enablePasscode))) {

                                    Intent i = new Intent(getApplicationContext(), SettingActivity.class);
                                    startActivity(i);
                                } else {


                                    passcodeSecurity(getApplicationContext());
                                }
                            }
                        });


                        generateKey();

                        if (cipherInit()) {

                            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                            FingerprintHandler helper = new FingerprintHandler(getApplicationContext());

                            helper.startAuth(fingerprintManager, cryptoObject);


                        } else {

                            Toast.makeText(getApplicationContext(), getString(R.string.tryAgain), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }


    }


    private void save_and_send_signature() {
        LL_validate.setEnabled(true);
        try {
            String packageName = getCurrentInputEditorInfo().packageName;
            pack = packageName;
            SampleActivity.packageName = pack;
            if (pack.equals("")) {
                Toast.makeText(getApplicationContext(), "sorry package name  empty", Toast.LENGTH_SHORT).show();
            }
            // signatureView.onClickUndo();
            if (gifSupported & signatureView.paths.size() == 0) {
                commitGifImage(getSavedSignatureUri(), "image/png");
//                signatureView.clear();

                signatureView.clear();
                validateText.setText("VALIDATE SIGNATURE");
                unregisterReceiver(Finepayed);
                unregisterReceiver(FingerPrintfailed);
                unregisterReceiver(stopProgressBar);
                unregisterReceiver(fingerPrintReciever);
            } else {
                //gmail worked
                if (packageName.equals("com.google.android.gm")) {
                    //  ACTION SEND MULTIPLE
                    try {

                        uris.add(getSavedSignatureUri());

                        if (excecute) {
                            Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                            intent.setType("text/html");
                            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                            intent.setPackage("com.google.android.gm");
//                            char[] recipients = {};
                            //  intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK)
                            intent.putExtra(android.content.Intent.EXTRA_STREAM, uris).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            signatureView.clear();
                            validateText.setText("VALIDATE SIGNATURE");
//                            unregisterReceiver(Finepayed);
//                            unregisterReceiver(stopProgressBar);
//                            unregisterReceiver(FingerPrintfailed);
//                            //       unregisterReceiver(backPressed);
//                            unregisterReceiver(fingerPrintReciever);
//

                            uris.clear();
                            //trying
//                            List<ApplicationInfo> packages;
//                            PackageManager pm;
//                            pm = getPackageManager();
//                            //get a list of installed apps.
//                            packages = pm.getInstalledApplications(0);
//for(int i=0;i<packages.size();i++){
//
//    if(packages.get(i).equals("com.app.signidapp")){
//
//        Toast.makeText(getApplicationContext(),"kjdgklsjgos",Toast.LENGTH_SHORT).show();
//
//
//
//    }
//}
//                            ActivityManager mActivityManager = (ActivityManager)getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
//
//                            for (ApplicationInfo packageInfo : packages) {
//                                if((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM)==1)continue;
//                                if(packageInfo.packageName.equals("com.app.signidapp")) continue;
//                                mActivityManager.killBackgroundProcesses(packageInfo.packageName);
//                            }
//
//


                        } else {

                            Toast.makeText(getApplicationContext(), "iamnotexcecute", Toast.LENGTH_SHORT).show();

                        }
                    } catch (Exception ex) {
                        Log.i("exception", ex.toString());
                        Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
                    }
                } else if (packageName.equals(messagePackage)) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/*");
                    intent.setPackage(messagePackage);
                    intent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    signatureView.clear();
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(stopProgressBar);
                    unregisterReceiver(FingerPrintfailed);
                    // unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);
                } else if (packageName.equals("com.whatsapp.w4b")) {
                    try {

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setPackage("com.whatsapp");
                        intent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                        intent.setType("image/*");
                        startActivity(intent);
                        signatureView.onClickUndo();

                        unregisterReceiver(stopProgressBar);
                        validateText.setText("VALIDATE SIGNATURE");
                        unregisterReceiver(Finepayed);
                        unregisterReceiver(FingerPrintfailed);
                        //   unregisterReceiver(backPressed);
                        unregisterReceiver(fingerPrintReciever);

                        signatureView.clear();


                    } catch (Exception ex) {
//                        Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else if (packageName.equals("com.whatsapp")) {
                    try {

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setPackage("com.whatsapp");
                        intent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                        intent.setType("image/*");
                        startActivity(intent);
                        signatureView.onClickUndo();

                        unregisterReceiver(stopProgressBar);
                        validateText.setText("VALIDATE SIGNATURE");
                        unregisterReceiver(Finepayed);
                        unregisterReceiver(FingerPrintfailed);
                        //   unregisterReceiver(backPressed);
                        unregisterReceiver(fingerPrintReciever);

                        signatureView.clear();


                    } catch (Exception ex) {
//                        Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else if (packageName.equals("com.Slack")) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setType("text/html");
                    intent.setPackage("com.Slack");
                    startActivity(intent);
                    signatureView.clear();
                    unregisterReceiver(stopProgressBar);
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(FingerPrintfailed);
                    // unregisterReceiver(backPressed);

                    unregisterReceiver(fingerPrintReciever);
                } else if (packageName.equals("com.skype.raider")) {
                    Intent intent = new Intent(Intent.ACTION_SEND);


                    //try
                    intent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri());
                    intent.setType("text/html");
                    intent.setPackage("com.skype.raider");

                    startActivity(intent);
                    signatureView.onClickUndo();
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(FingerPrintfailed);
                    unregisterReceiver(stopProgressBar);
                    // unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);

                } else if (packageName.equals("com.facebook.orca")) {

                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
//
                    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                    shareIntent.setPackage("com.facebook.orca");
                    startActivity(shareIntent);
                    signatureView.clear();
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(stopProgressBar);
                    unregisterReceiver(FingerPrintfailed);
                    //unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);

                } else if (packageName.equals("com.facebook.katana")) {

                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
//
                    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                    shareIntent.setPackage("com.facebook.katana");
                    startActivity(shareIntent);
                    signatureView.onClickUndo();
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(stopProgressBar);
                    unregisterReceiver(FingerPrintfailed);
                    //   unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);

                } else if (packageName.equals("com.twitter.android")) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.setPackage("com.twitter.android");
                    startActivity(shareIntent);
                    signatureView.clear();
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(stopProgressBar);
                    unregisterReceiver(FingerPrintfailed);
                    //   unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);
                } else if (packageName.equals("com.instagram.android")) {
                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("image/*");
                        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, getSavedSignatureUri());
                        shareIntent.setPackage("com.instagram.android");
                        getApplicationContext().startActivity(shareIntent);
                        signatureView.clear();
                        validateText.setText("VALIDATE SIGNATURE");
                        unregisterReceiver(Finepayed);
                        unregisterReceiver(stopProgressBar);
                        unregisterReceiver(FingerPrintfailed);
                        //      unregisterReceiver(backPressed);
                        unregisterReceiver(fingerPrintReciever);

                    } catch (Exception e) {

//                        Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();

                    }

                } else if (packageName.equals("org.telegram.messenger")) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.setPackage("org.telegram.messenger");
                    startActivity(shareIntent);

                    signatureView.clear();
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(stopProgressBar);
                    unregisterReceiver(FingerPrintfailed);
                    //     unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);
                } else if (packageName.equals("com.linkedin.android")) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.setPackage("com.linkedin.android");
                    startActivity(shareIntent);
                    signatureView.clear();
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(FingerPrintfailed);
                    unregisterReceiver(stopProgressBar);
                    // unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);
                } else if (packageName.equals("com.snapchat.android")) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.setPackage("com.snapchat.android");
                    startActivity(shareIntent);
                    signatureView.clear();
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(stopProgressBar);
                    unregisterReceiver(FingerPrintfailed);
                    //       unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);
                } else if (packageName.equals("com.quora.android")) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.setPackage("com.quora.android");
                    startActivity(shareIntent);
                    signatureView.clear();
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(FingerPrintfailed);
                    unregisterReceiver(stopProgressBar);
                    //     unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);
                } else if (packageName.equals("com.quora.android")) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, getSavedSignatureUri()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                    shareIntent.setPackage("com.quora.android");
                    startActivity(shareIntent);
                    signatureView.clear();
                    validateText.setText("VALIDATE SIGNATURE");
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(FingerPrintfailed);
                    unregisterReceiver(stopProgressBar);
                    // unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);
                } else {
                    Toast.makeText(getApplicationContext(), "Sorry! this feature is not supported for current application", Toast.LENGTH_SHORT).show();
                    validateText.setText("VALIDATE SIGNATURE");
                    signatureView.clear();
                    unregisterReceiver(Finepayed);
                    unregisterReceiver(FingerPrintfailed);
                    unregisterReceiver(stopProgressBar);
                    //     unregisterReceiver(backPressed);
                    unregisterReceiver(fingerPrintReciever);
                }

            }
        } catch (Exception ex) {
            System.out.println("ERROR=====" + ex);
        }


    }

    @Override
    public void onFinishInput() {
        super.onFinishInput();
        mComposing.setLength(0);
        System.out.println("M CALLED FROM onFinishInput()");
//        LL_temp.setVisibility(View.GONE);
        /* signatureView.clear();*/
        setCandidatesViewShown(false);
        mCurKeyboard = mQwertyKeyboard;
        /*if (latinKeyboardView != null)
        {
            latinKeyboardView.closing();
        }

        try {
            if (screenOnOffReceiver!=null)
            {
                unregisterReceiver(screenOnOffReceiver);
            }
        }
        catch (IllegalArgumentException e)
        {
            System.out.println("EROR==="+e);
            e.printStackTrace();
        }*/


    }

    /**
     * Use this to monitor key events being delivered to the application.
     * We get first crack at them, and can either resume them or let them
     * continue to the app.
     */

    private boolean translateKeyDown(int keyCode, KeyEvent event) {
        mMetaState = MetaKeyKeyListener.handleKeyDown(mMetaState,
                keyCode, event);
        int c = event.getUnicodeChar(MetaKeyKeyListener.getMetaState(mMetaState));
        mMetaState = MetaKeyKeyListener.adjustMetaAfterKeypress(mMetaState);
        InputConnection ic = getCurrentInputConnection();
        if (c == 0 || ic == null) {
            return false;
        }

        boolean dead = false;
        if ((c & KeyCharacterMap.COMBINING_ACCENT) != 0) {
            dead = true;
            c = c & KeyCharacterMap.COMBINING_ACCENT_MASK;
        }

        if (mComposing.length() > 0) {
            char accent = mComposing.charAt(mComposing.length() - 1);
            int composed = KeyEvent.getDeadChar(accent, c);
            if (composed != 0) {
                c = composed;
                mComposing.setLength(mComposing.length() - 1);
            }
        }

        onKey(c, null);

        return true;
    }

    /**
     * Use this to monitor key events being delivered to the application.
     * We get first crack at them, and can either resume them or let them
     * continue to the app.
     */


    private IBinder getToken() {

        System.out.println("M CALLED FROM");
        final Dialog dialog = getWindow();
        if (dialog == null) {
            return null;
        }
        final Window window = dialog.getWindow();
        if (window == null) {
            return null;
        }
        return window.getAttributes().token;
    }

    private void handleLanguageSwitch() {

        if (android.os.Build.VERSION.SDK_INT > 25) {
            InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
            imeManager.showInputMethodPicker();
            //   mInputMethodManager.switchToNextInputMethod(getToken(), false /* onlyCurrentIme */);
        } else
            mInputMethodManager.switchToNextInputMethod(getToken(), false /* onlyCurrentIme */);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //   unregisterReceiver(screenOnOffReceiver);

        System.out.println("ON DESTROY========");
    }

    @Override
    public void onFinishInputView(boolean finishingInput) {
        super.onFinishInputView(finishingInput);

        System.out.println("HELLO RITU===FROM ON FINISHED");
    }

    public void commitGifImage(Uri contentUri, String imageDescription) {

        InputContentInfoCompat inputContentInfo = new InputContentInfoCompat(contentUri, new ClipDescription(imageDescription, new String[]{"image/png"}), null);
        InputConnection inputConnection = getCurrentInputConnection();

        EditorInfo editorInfo = getCurrentInputEditorInfo();
        // editorInfo.imeOptions

        // editorInfo.notify();
        System.out.println("M EXECUTED==========" + contentUri);

        // editorInfo.contentMimeTypes.length
        int flags = 0;
        if (Build.VERSION.SDK_INT >= 25) {
            flags |= InputConnectionCompat.INPUT_CONTENT_GRANT_READ_URI_PERMISSION;
        }
        boolean isCommit = InputConnectionCompat.commitContent(
                inputConnection, editorInfo, inputContentInfo, flags, null);
        InputBinding inputBinding = getCurrentInputBinding();
        //editorInfo.notify();
        System.out.println("IS COMMIT====" + inputBinding);
    }

    public Uri insertContentProvider(ContentResolver cr, String imgType, File filepath) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "signature");
        values.put(MediaStore.Images.Media.DISPLAY_NAME, "signature");
        values.put(MediaStore.Images.Media.DESCRIPTION, "");
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/" + imgType);
        values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.DATA, filepath.toString());
        return cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    @Override
    public void onPress(int primaryCode) {
        Log.e(TAG, "onPress: " + primaryCode);

        kv.setPreviewEnabled(true);

        if (primaryCode == -1) {

            kv.setPreviewEnabled(false);
            if (!arrowPressed) {
                for (int i = 0; i < kv.getKeyboard().getKeys().size(); i++) {
                    if (kv.getKeyboard().getKeys().get(i).modifier) {
                        arrowPressed = true;

                        kv.getKeyboard().getKeys().get(i).icon = getResources().getDrawable(R.drawable.caps1);
                    }
                }
            } else {

                for (int i = 0; i < kv.getKeyboard().getKeys().size(); i++) {
                    if (kv.getKeyboard().getKeys().get(i).modifier) {
                        arrowPressed = false;

                        kv.getKeyboard().getKeys().get(i).icon = getResources().getDrawable(R.drawable.caps_xxhdpi);
                    }
                }
            }

        }
        if (primaryCode == -2 || primaryCode == 10 || primaryCode == -5 || primaryCode == -101
                || primaryCode == -100 || primaryCode == 32) {
            kv.setPreviewEnabled(false);
        }

        if (primaryCode == -101) {
            handleLanguageSwitch();
        }
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        return super.onKeyLongPress(keyCode, event);

    }

    @Override
    public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
        return super.onKeyMultiple(keyCode, count, event);

    }

    @Override
    public void onRelease(int primaryCode) {
        Log.e(TAG, "onRelease: " + primaryCode);


    }

    @Override
    public void onText(CharSequence text) {
        InputConnection ic = getCurrentInputConnection();
        if (ic == null) return;
        ic.beginBatchEdit();
        if (mComposing.length() > 0) {
            commitTyped(ic);
        }
        ic.commitText(text, 0);
        ic.endBatchEdit();
        updateShiftKeyState(getCurrentInputEditorInfo());
    }

    public void swipeRight() {
        if (mCompletionOn) {
            pickDefaultCandidate();
        }
    }

    public void swipeLeft() {

        handleBackspace();
    }

    public void swipeDown() {


        handleClose();
    }

    public void swipeUp() {
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // If we want to do transformations on text being entered with a hard
        // keyboard, we need to process the up events to update the meta key
        // state we are tracking.
        if (PROCESS_HARD_KEYS) {
            if (mPredictionOn) {
                mMetaState = MetaKeyKeyListener.handleKeyUp(mMetaState,
                        keyCode, event);
            }
        }

        return super.onKeyUp(keyCode, event);
    }





    /*THIS FINGER PRINT*/

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                // The InputMethodService already takes care of the back
                // key for us, to dismiss the input_try method if it is shown.
                // However, our keyboard could be showing a pop-up window
                // that back should dismiss, so we first allow it to do that.
                if (event.getRepeatCount() == 0 && kv != null) {
                    if (kv.handleBack()) {
                        return true;
                    }
                }
                break;

            case KeyEvent.KEYCODE_DEL:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
                // Special handling of the delete key: if we currently are
                // composing text for the user, we want to modify that instead
                // of let the application to the delete itself.
                if (mComposing.length() > 0) {
                    onKey(Keyboard.KEYCODE_DELETE, null);
                    return true;
                }
                break;


            case KeyEvent.KEYCODE_ENTER:
                // Let the underlying text editor always handle these.
                return false;

            default:
                // For all other keys, if we want to do transformations on
                // text being entered with a hard keyboard, we need to process
                // it and do the appropriate action.
                if (PROCESS_HARD_KEYS) {
                    if (keyCode == KeyEvent.KEYCODE_SPACE
                            && (event.getMetaState() & KeyEvent.META_ALT_ON) != 0) {
                        // A silly example: in our input_try method, Alt+Space
                        // is a shortcut for 'android' in lower case.
                        InputConnection ic = getCurrentInputConnection();
                        if (ic != null) {
                            // First, tell the editor that it is no longer in the
                            // shift state, since we are consuming this.
                            ic.clearMetaKeyStates(KeyEvent.META_ALT_ON);
                            keyDownUp(KeyEvent.KEYCODE_A);
                            keyDownUp(KeyEvent.KEYCODE_N);
                            keyDownUp(KeyEvent.KEYCODE_D);
                            keyDownUp(KeyEvent.KEYCODE_R);
                            keyDownUp(KeyEvent.KEYCODE_O);
                            keyDownUp(KeyEvent.KEYCODE_I);
                            keyDownUp(KeyEvent.KEYCODE_D);
                            // And we consume this event.
                            return true;
                        }
                    }
                    if (mPredictionOn && translateKeyDown(keyCode, event)) {
                        return true;
                    }
                }
        }

        return super.onKeyDown(keyCode, event);
    }

    public void pickSuggestionManually(int index) {
        if (mCompletionOn && mCompletions != null && index >= 0
                && index < mCompletions.length) {
            CompletionInfo ci = mCompletions[index];
            getCurrentInputConnection().commitCompletion(ci);
            if (mCandidateView != null) {
                mCandidateView.clear();
            }
            updateShiftKeyState(getCurrentInputEditorInfo());
        } else if (mComposing.length() > 0) {
            // If we were generating candidate suggestions for the current
            // text, we would commit one of them here.  But for this sample,
            // we will just commit the current text.
            commitTyped(getCurrentInputConnection());
        }
    }

    public void pickDefaultCandidate() {
        pickSuggestionManually(0);
    }

    private Uri getSavedSignatureUri() {
        String pic_name = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        Uri imageUri = null;

        try {
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(signature_panel.getWidth(), signature_panel.getHeight(), Bitmap.Config.RGB_565);
            }
            Canvas canvas = new Canvas(bitmap);
            signature_panel.draw(canvas);
            File outputFile = new File(Environment.getExternalStorageDirectory(), "/DCIM/" + pic_name + ".png");
            FileOutputStream mFileOutStream = new FileOutputStream(outputFile);


            // Convert the output file to Image such as .png
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
            mFileOutStream.flush();
            mFileOutStream.close();

            System.out.println("PATH====" + Uri.parse(outputFile.getAbsolutePath()));


            // getContentResolver()
            imageUri = insertContentProvider(getContentResolver(), "png", outputFile);
            ContentResolver cR = getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String type = cR.getType(imageUri);

        } catch (Exception ex) {
            System.out.println("Exception ex====" + ex);
        }
        excecute = true;
        return imageUri;


    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();

            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {


            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            Log.i("whyreturningfalse", e.toString());
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    @Override
    public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);
//        for(int i=0;i<mQwertyKeyboard.getKeys().size();i++){
//            mQwertyKeyboard.getKeys().get(i).label.toString().toUpperCase();
//
//        }
        // Reset our state.  We want to do this even if restarting, because
        // the underlying state of the text editor could have changed in any way.
        mComposing.setLength(0);


        if (!restarting) {
            // Clear shift states.
            mMetaState = 0;
        }

        mPredictionOn = false;
        mCompletionOn = false;
        mCompletions = null;

        // We are now going to initialize our state based on the type of
        // text being edited.
        switch (attribute.inputType & InputType.TYPE_MASK_CLASS) {
            case InputType.TYPE_CLASS_NUMBER:
            case InputType.TYPE_CLASS_DATETIME:

                mCurKeyboard = mSymbolsKeyboard;
                break;

            case InputType.TYPE_CLASS_PHONE:

                mCurKeyboard = mSymbolsKeyboard;
                break;

            case InputType.TYPE_CLASS_TEXT:

                mCurKeyboard = mQwertyKeyboard;
                mPredictionOn = true;

                // We now look for a few special variations of text that will
                // modify our behavior.
                int variation = attribute.inputType & InputType.TYPE_MASK_VARIATION;
                if (variation == InputType.TYPE_TEXT_VARIATION_PASSWORD ||
                        variation == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    // Do not display predictions / what the user is typing
                    // when they are entering a password.
                    mPredictionOn = false;
                }

                if (variation == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                        || variation == InputType.TYPE_TEXT_VARIATION_URI
                        || variation == InputType.TYPE_TEXT_VARIATION_FILTER) {
                    // Our predictions are not useful for e-mail addresses
                    // or URIs.
                    mPredictionOn = false;
                }

                if ((attribute.inputType & InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE) != 0) {
                    // If this is an auto-complete text view, then our predictions
                    // will not be shown and instead we will allow the editor
                    // to supply their own.  We only show the editor's
                    // candidates when in fullscreen mode, otherwise relying
                    // own it displaying its own UI.
                    mPredictionOn = false;
                    mCompletionOn = isFullscreenMode();
                }

                // We also want to look at the current state of the editor
                // to decide whether our alphabetic keyboard should start out
                // shifted.
                updateShiftKeyState(attribute);
                break;

            default:
                // For all unknown input_try types, default to the alphabetic
                // keyboard with no special features.
                mCurKeyboard = mQwertyKeyboard;

                updateShiftKeyState(attribute);
        }

        // Update the label on the enter key, depending on what the application
        // says it will do.
        mCurKeyboard.setImeOptions(getResources(), attribute.imeOptions);
    }

    private void updateShiftKeyState(EditorInfo attr) {
        if (attr != null
                && kv != null && mQwertyKeyboard == kv.getKeyboard()) {

            int caps = 0;
            EditorInfo ei = getCurrentInputEditorInfo();
            if (ei != null && ei.inputType != InputType.TYPE_NULL) {
                caps = getCurrentInputConnection().getCursorCapsMode(attr.inputType);
            }
//            kv.setShifted(mCapsLock || caps != 0);


        }
    }

    //trying , int[] keyCodes
    public void onKey(int primaryCode, int[] keyCodes) {
        if (isWordSeparator(primaryCode)) {
            // Handle separator
            if (mComposing.length() > 0) {
                commitTyped(getCurrentInputConnection());
            }
            sendKey(primaryCode);
            updateShiftKeyState(getCurrentInputEditorInfo());
        } else if (primaryCode == Keyboard.KEYCODE_DELETE) {
            handleBackspace();
        } else if (primaryCode == Keyboard.KEYCODE_SHIFT) {

            handleShift();
        } else if (primaryCode == Keyboard.KEYCODE_CANCEL) {
            handleClose();
            return;
        } else if (primaryCode == LatinKeyboardView.KEYCODE_LANGUAGE_SWITCH) {
            handleLanguageSwitch();
            return;
        } else if (primaryCode == LatinKeyboardView.KEYCODE_OPTIONS) {

            kv.setFocusableInTouchMode(true);
            handleShift();
           /* signature_panel.removeAllViews();
            signature_panel.addView(signatureView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);*/
            //  signature_panel.removeAllViews();

            LL_keyboard_holder.setVisibility(View.GONE);

            // signature_panel.addView(signatureView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            LL_pad_holder.setVisibility(View.VISIBLE);
            // mInputView2.addView(signatureView);

            // Show a menu or somethin'
        } else if (primaryCode == Keyboard.KEYCODE_MODE_CHANGE && kv != null) {
            Keyboard current = kv.getKeyboard();
            if (current == mSymbolsKeyboard || current == mSymbolsShiftedKeyboard) {

                kv.setKeyboard(mQwertyKeyboard);

                //trying for try
                // kv.setPopupOffset(120,120);
                //setLatinKeyboard(mQwertyKeyboard);
            } else {
                kv.setKeyboard(mSymbolsKeyboard);
                // setLatinKeyboard(mSymbolsKeyboard);
                mSymbolsKeyboard.setShifted(false);
            }
        } else {
            handleCharacter(primaryCode, keyCodes);
        }
    }

    private void handleClose() {
        commitTyped(getCurrentInputConnection());
        requestHideSelf(0);
        kv.closing();
    }

    private void handleShift() {
        if (kv == null) {
            return;
        }

        Keyboard currentKeyboard = kv.getKeyboard();
        if (mQwertyKeyboard == currentKeyboard) {
            //checkToggleCapsLock();
            kv.setShifted(mCapsLock || !kv.isShifted());
            //  kv.setShifted(mCapsLock || !kv.isShifted());

        } else if (currentKeyboard == mSymbolsKeyboard) {
            mSymbolsKeyboard.setShifted(true);
            setLatinKeyboard(mSymbolsShiftedKeyboard);

            mSymbolsShiftedKeyboard.setShifted(true);
        } else if (currentKeyboard == mSymbolsShiftedKeyboard) {
            mSymbolsShiftedKeyboard.setShifted(false);
            setLatinKeyboard(mSymbolsKeyboard);

            mSymbolsKeyboard.setShifted(false);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void setLatinKeyboard(LatinKeyboard nextKeyboard) {
        final boolean shouldSupportLanguageSwitchKey =
                mInputMethodManager.shouldOfferSwitchingToNextInputMethod(getToken());
        nextKeyboard.setLanguageSwitchKeyVisibility(shouldSupportLanguageSwitchKey);
        kv.setKeyboard(nextKeyboard);
    }

    private void handleBackspace() {
        final int length = mComposing.length();
        System.out.println("LENGTH " + length);

        if (length > 1) {
            mComposing.delete(length - 1, length);
            getCurrentInputConnection().setComposingText(mComposing, 1);

        } else if (length > 0) {
            mComposing.setLength(0);
            getCurrentInputConnection().commitText("", 0);

        } else {
            keyDownUp(KeyEvent.KEYCODE_DEL);
        }
        updateShiftKeyState(getCurrentInputEditorInfo());
    }

    private void sendKey(int keyCode) {
        switch (keyCode) {
            case '\n':
                keyDownUp(KeyEvent.KEYCODE_ENTER);
                break;
            default:
                if (keyCode >= '0' && keyCode <= '9') {
                    keyDownUp(keyCode - '0' + KeyEvent.KEYCODE_0);
                } else {
                    getCurrentInputConnection().commitText(String.valueOf((char) keyCode), 1);
                }
                break;
        }
    }

    public boolean isWordSeparator(int code) {
        String separators = getWordSeparators();
        return separators.contains(String.valueOf((char) code));
    }

    private void keyDownUp(int keyEventCode) {

        getCurrentInputConnection().sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, keyEventCode));
        getCurrentInputConnection().sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, keyEventCode));

    }

    private String getWordSeparators() {
        return mWordSeparators;
    }

    private void handleCharacter(int primaryCode, int[] keyCodes) {
        if (isInputViewShown()) {
            if (kv.isShifted()) {
                primaryCode = Character.toUpperCase(primaryCode);
            }
        }
        if (isAlphabet(primaryCode) && mPredictionOn) {
            mComposing.append((char) primaryCode);
            getCurrentInputConnection().setComposingText(mComposing, 1);
            updateShiftKeyState(getCurrentInputEditorInfo());
            //  updateCandidates();
        } else {

            System.out.println("OUTPUT  " + mComposing);

            mComposing.append((char) primaryCode);
            getCurrentInputConnection().commitText(
                    mComposing, 1);
        }
    }

    /**
     * Helper to determine if a given character code is alphabetic.
     */
    private boolean isAlphabet(int code) {
        if (Character.isLetter(code)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Helper function to commit any text being composed in to the editor.
     */
    private void commitTyped(InputConnection inputConnection) {
        if (mComposing.length() > 0) {
            inputConnection.commitText(mComposing, mComposing.length());
            mComposing.setLength(0);

        }
    }

    @Override
    public void onUpdateSelection(int oldSelStart, int oldSelEnd,
                                  int newSelStart, int newSelEnd,
                                  int candidatesStart, int candidatesEnd) {
        super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd,
                candidatesStart, candidatesEnd);

        // If the current selection in the text view changes, we should
        // clear whatever candidate text we have.
        if (mComposing.length() > 0 && (newSelStart != candidatesEnd
                || newSelEnd != candidatesEnd)) {
            mComposing.setLength(0);
            //updateCandidates();
            InputConnection ic = getCurrentInputConnection();
            if (ic != null) {
                ic.finishComposingText();
            }
        }
    }
//
//    private boolean tryLockedorNot() {
//        String LOCKSCREEN_UTILS = "com.android.internal.widget.LockPatternUtils";
//        try {
//            Class<?> lockUtilsClass = Class.forName(LOCKSCREEN_UTILS);
//            // "this" is a Context, in my case an Activity
//            Object lockUtils = lockUtilsClass.getConstructor(Context.class).newInstance(this);
//
//            Method method = lockUtilsClass.getMethod("getActivePasswordQuality");
//
//            int lockProtectionLevel = (Integer) method.invoke(lockUtils); // Thank you esme_louise for the cast hint
//
//            if (lockProtectionLevel >= DevicePolicyManager.PASSWORD_QUALITY_NUMERIC) {
//                return true;
//            }
//        } catch (Exception e) {
//            Log.e("reflectInternalUtils", "ex:" + e);
//        }
//
//        return false;
//    }
//
//    private boolean isDeviceFacedLocked() {
//        String LOCKSCREEN_UTILS = "com.android.internal.widget.LockPatternUtils";
//        try {
//            Class<?> lockUtilsClass = Class.forName(LOCKSCREEN_UTILS);
//            // "this" is a Context, in my case an Activity
//            Object lockUtils = lockUtilsClass.getConstructor(Context.class).newInstance(this);
//
//            Method method = lockUtilsClass.getMethod("getActivePasswordQuality");
//
//            int lockProtectionLevel = (Integer) method.invoke(lockUtils); // Thank you esme_louise for the cast hint
//
//            if (lockProtectionLevel >= DevicePolicyManager.PASSWORD_QUALITY_NUMERIC) {
//                return true;
//            }
//        } catch (Exception e) {
//            Log.e("reflectInternalUtils", "ex:" + e);
//        }
//
//        return false;
//    }
//
//    private void checkToggleCapsLock() {
//        long now = System.currentTimeMillis();
//        if (mLastShiftTime + 800 > now) {
//            mCapsLock = !mCapsLock;
//            mLastShiftTime = 0;
//        } else {
//            mLastShiftTime = now;
//        }
//    }

    public void passcodeSecurity(Context context) {
        LL_temp.setVisibility(View.GONE);
        Intent i = new Intent(getApplicationContext(), SampleActivity.class);
        i.putExtra(Constants.PASSCODE, "alreadysetted");
        i.putExtra(Constants.COMINGFROMSERVICE, true);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);


    }

    public class ScreenOnOffReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                System.out.println("I AM FROM=====ACTION_SCREEN_ON");
            }
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                System.out.println("I AM FROM=====ACTION_SCREEN_OFF");
            }
            if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
                System.out.println("I AM FROM=====ACTION_USER_PRESENT");
                validateText.setText(getString(R.string.validating));
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        System.out.println("CURRENT KEYBOARD=========" + Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD));
                        mProgressbar2.setVisibility(View.GONE);
                        save_and_send_signature();

                    }
                }, Constants.THREAD_TIME);

            }

        }
    }

    public class FingerPrintReciever extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("M TRIGGERD=====   FINGER PRIJNT");
            LL_temp.setVisibility(View.GONE);

            validateText.setText(getString(R.string.validating));

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    System.out.println("CURRENT KEYBOARD=========" + Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD));
                    mProgressbar2.setVisibility(View.GONE);


                    save_and_send_signature();

                }
            }, Constants.THREAD_TIME);

        }
    }

}