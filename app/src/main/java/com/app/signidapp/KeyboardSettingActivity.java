package com.app.signidapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;


import com.app.signidapp.session.SignIDAppSession;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KeyboardSettingActivity extends AppCompatActivity {


    @BindView(R.id.add_keyboard_btn)
    Button add_keyboard_btn;

    SignIDAppSession session;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyboard_setting);



        ButterKnife.bind(this);
        session=new SignIDAppSession(this);
        add_keyboard_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                imeManager.showInputMethodPicker();*/


                startActivityForResult(new Intent(Settings.ACTION_INPUT_METHOD_SETTINGS), 0);


            }
        });







    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
       // unregisterReceiver(inputMethodChangeReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        if (requestCode == 0)
        {
            InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
            for (int i=0;i<imeManager.getEnabledInputMethodList().size();i++)
            {
               if( imeManager.getEnabledInputMethodList().get(i).getPackageName().equals("com.app.signidapp"))
               {
                    session.setKeySetting("yes");
                    startActivity(new Intent(KeyboardSettingActivity.this, KeyboardChoosing.class));
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    finish();
               }
            }

        }

       /* IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Intent.);
        this.registerReceiver(br, filter);*/
    }


    /*public class InputMethodChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            System.out.println("M Called");

            String action = intent.getAction();
            if (action.equals(Intent.ACTION_INPUT_METHOD_CHANGED))
            {
                InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                for (int i=0;i<imeManager.getEnabledInputMethodList().size();i++)
                {
                    if( imeManager.getEnabledInputMethodList().get(i).getPackageName().equals("com.app.signidapp"))
                    {
                        session.setKeySetting("yes");
                        startActivity(new Intent(KeyboardSettingActivity.this, KeyboardChoosing.class));
                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                        finish();
                    }
                }
            }
        }
    }*/
}
