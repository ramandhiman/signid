package com.app.signidapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.app.signidapp.font.GothamMediumButton;
import com.app.signidapp.model.SignUpResponse;
import com.app.signidapp.utils.CommonLoadingDialog;
import com.app.signidapp.utils.CommonValidationDialog;
import com.app.signidapp.utils.Content;
import com.app.signidapp.utils.EmailValidator;
import com.google.gson.Gson;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends AppCompatActivity {



    @BindView(R.id.backBtn)
    ImageView backBtn;
    @BindView(R.id.inputUserName)
    EditText inputUserName;
    @BindView(R.id.LL_username_container)
    LinearLayout LLUsernameContainer;
    @BindView(R.id.inputEmail)
    EditText inputEmail;
    @BindView(R.id.LL_email_container)
    LinearLayout LLEmailContainer;
    @BindView(R.id.inputPass)
    EditText inputPass;
    @BindView(R.id.LL_pass_container)
    LinearLayout LLPassContainer;
    @BindView(R.id.inputCPass)
    EditText inputCPass;
    @BindView(R.id.LL_cpass_container)
    LinearLayout LLCpassContainer;
    @BindView(R.id.LL_signupPanelContainer)
    LinearLayout LLSignupPanelContainer;
    @BindView(R.id.LL_container)
    LinearLayout LLContainer;
    @BindView(R.id.btn_sign_up)
    GothamMediumButton btnSignUp;
    @BindView(R.id.LL_signUp_button_contianer)
    LinearLayout LLSignUpButtonContianer;
    @BindView(R.id.LL_signup_info_container)
    LinearLayout LLSignupInfoContainer;

    @BindView(R.id.LL_Toc_container)
    LinearLayout LL_Toc_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        LLSignupInfoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                finish();
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
               // Toast.makeText(SignUpActivity.this, "hii", Toast.LENGTH_SHORT).show();

                if(valdateUserName() && validateEmail() &&  validatePassword() && validateConfirmPassword()&&validatePasswordmatch())
                {
                    //Toast.makeText(SignUpActivity.this, "OK", Toast.LENGTH_SHORT).show();
                   // CommonValidationDialog.showLoadingDialog(SignUpActivity.this,"");
                   httpSignUp();
                }


            }
        });

        LL_Toc_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://signid.com/terms"));
                startActivity(intent);
            }
        });






    }


    private boolean validateEmail()
    {
        if (inputEmail.getText().toString().trim().equals(""))
        {
            CommonValidationDialog.showLoadingDialog(SignUpActivity.this, "Please fill the empty field");
            return false;
        }
        else {

            if (new EmailValidator().validateEmail(inputEmail.getText().toString().trim()))
            {
                return true;
            }
            else {
                CommonValidationDialog.showLoadingDialog(SignUpActivity.this, "Please enter a valid email");
                return false;
            }

        }

    }
    private boolean valdateUserName()
    {
        if(inputUserName.getText().toString().trim().equals(""))
        {
            CommonValidationDialog.showLoadingDialog(SignUpActivity.this,"Please fill the empty field");

           // Toast.makeText(this, "Username should not be empty", Toast.LENGTH_SHORT).show();
            return  false;
        }
        return true;
    }
    private boolean validatePassword()
    {
        if (inputPass.getText().toString().equals(""))
        {
            CommonValidationDialog.showLoadingDialog(SignUpActivity.this,"Please fill the empty field");
            return false;
        }
        return true;
    }
    private boolean validateConfirmPassword()
    {
        if (inputCPass.getText().toString().equals(""))
        {
            CommonValidationDialog.showLoadingDialog(SignUpActivity.this,"Please fill the empty field");
            return false ;
        }
        return true;
    }

    private boolean validatePasswordmatch()
    {
        if(!inputPass.getText().toString().trim().equals(inputCPass.getText().toString().trim()))
        {
            CommonValidationDialog.showLoadingDialog(SignUpActivity.this,"Password not match");
            return false ;
        }
        return true;
    }







    private void httpSignUp()
    {
        try
        {
            String url = Content.baseURL + "SignUp.php?username="+inputUserName.getText().toString().trim()+"&email="+inputEmail.getText().toString().trim()+"&password="+inputPass.getText()+"";

            System.out.println("URL==="+url);


            CommonLoadingDialog.showLoadingDialog(SignUpActivity.this,"Sign Up...");



            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            CommonLoadingDialog.closeLoadingDialog();


                            System.out.println("RESPONSE====="+response);



                            try
                            {
                                JSONObject jsonObject=new JSONObject(response);
                                String status= jsonObject.getString("status");
                                String message=jsonObject.getString("message");

                                if(status.equals("1"))
                                {
                                    Gson gson=new Gson();
                                    SignUpResponse signUpResponse=gson.fromJson(response,SignUpResponse.class);
                                    Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_SHORT).show();

                                    startActivity(new Intent(SignUpActivity.this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    finish();

                                }
                                else
                                {
                                    Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_SHORT).show();
                                }


                            }
                            catch (Exception ex)
                            {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR=="+ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR=="+error.toString());
                        }
                    }) ;

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SignUpActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        }
        catch (Exception ex)
        {
            System.out.println("OUTPUT ======ERROR=="+ex.toString());
        }
    }
}
