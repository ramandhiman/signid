package com.app.signidapp.app;

/**
 * Created by RITUPARNA on 6/13/2018.
 */

public class SignIdSingleton
{
    private static SignIdSingleton signIdAppSingleton;
    private boolean hasSoftKey;
    private Object FingerPrintReciever;

    public Object getFingerPrintReciever() {
        return FingerPrintReciever;
    }

    public void setFingerPrintReciever(Object fingerPrintReciever) {
        FingerPrintReciever = fingerPrintReciever;
    }


    private SignIdSingleton()
    {

    }
    public static SignIdSingleton getInstance()
    {
        if(signIdAppSingleton==null)
        {
            signIdAppSingleton=new SignIdSingleton();
        }
        return signIdAppSingleton;
    }

    public boolean isHasSoftKey()
    {
        return hasSoftKey;
    }

    public void setHasSoftKey(boolean hasSoftKey)
    {
        this.hasSoftKey = hasSoftKey;
    }
}
