package com.app.signidapp;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.signidapp.model.FbSignInData;
import com.app.signidapp.model.FbSignInResponse;
import com.app.signidapp.session.SignIDAppSession;
import com.app.signidapp.utils.CommonLoadingDialog;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

public class ContinueActivity extends AppCompatActivity {


    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int RC_SIGN_IN = 234;
    // @BindView(R.id.textSkip)
    TextView textSkip;
    // @BindView(R.id.LL_fb_login_container)
    LinearLayout LL_fb_login_container;
    // @BindView(R.id.LL_gmail_login_container)
    LinearLayout LL_gmail_login_container;
    // @BindView(R.id.LL_email_login_container)
    LinearLayout LL_email_login_container;
    LoginButton fb_origin_login_button;
    LinearLayout LL_textContainer1;
    LinearLayout LL_login_container;
    ProgressBar progress_bar;
    String[] permissionsRequired = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    CallbackManager callbackManager;
    GoogleSignInClient mGoogleSignInClient;
    FirebaseAuth mAuth;
    FirebaseUser user;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private AlertDialog.Builder alertDialogBuilder;
    private String fbUserID;
    private String fbToken;
    private String str_facebookid;
    private String str_facebookname;
    private String imageUrl;
    private String fb_email;
    private String google_email;
    private String google_image_url;
    private String google_uid;
    private GoogleApiClient mGoogleApiClient;
    private SignIDAppSession session;


    private AlertDialog.Builder alertDialogBuilder2;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        FacebookSdk.sdkInitialize(getApplicationContext());
        //  FacebookSdk.sdkInitialize(getApplicationContext());

        /*Facebook Integration*/
        FacebookSdk.sdkInitialize(getApplicationContext());
        //  callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_continue);
       /* requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
*/


        initView();
        getKeyHashes();


        /**/

        googleLoginConfig();


        textSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (session.getKeyShowSetting().equals("yes")) {

                    startActivity(new Intent(ContinueActivity.this, SettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    finish();

//.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    //Toast.makeText(ContinueActivity.this, "Hiiii", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(ContinueActivity.this, KeyboardSettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    finish();
                }

            }
        });

        LL_gmail_login_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                LL_login_container.setEnabled(false);
                signIn();

            }
        });

        LL_fb_login_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                Intent i=new Intent(ContinueActivity.this,Main3Activity.class);
//                startActivity(i);
                loginWithFacebook();

            }
        });


        LL_email_login_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContinueActivity.this, SignUpActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);


            }
        });
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        permissionControl();


        fb_origin_login_button.setReadPermissions(Arrays.asList("email"));


        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        fb_origin_login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                System.out.println("RESUTL======" + loginResult.getRecentlyGrantedPermissions());
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();
                displayMessage(profile);
                getUserDetails(loginResult);
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("ERROR=======" + exception);
                // App code
            }
        });


        LL_textContainer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContinueActivity.this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                finish();
            }
        });

    }

    private void googleLoginConfig() {
        mAuth = FirebaseAuth.getInstance();

        //Then we need a GoogleSignInOptions object
        //And we need to build it as below
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        //Then we will get the GoogleSignInClient object from GoogleSignIn class
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }


    private void loginWithFacebook() {
        LoginManager.getInstance().logOut();
        fb_origin_login_button.performClick();
        // Toast.makeText(this, "HI", Toast.LENGTH_SHORT).show();
//        login_button_facebook.setReadPermissions("user_friends");

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                progress_bar.setVisibility(View.VISIBLE);
                //Toast.makeText(ContinueActivity.this, "Success by User", Toast.LENGTH_SHORT).show();
                getFbProfile(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
                //Toast.makeText(ContinueActivity.this, "Cancelled by User", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code

                System.out.println("ERROR=====" + exception);
                Toast.makeText(ContinueActivity.this, exception.toString(), Toast.LENGTH_SHORT).show();
                Log.i("", "");
            }
        });


    }

    private void initView() {
        callbackManager = CallbackManager.Factory.create();
        textSkip = (TextView) findViewById(R.id.textSkip);
        fb_origin_login_button = (LoginButton) findViewById(R.id.login_button);
        LL_fb_login_container = (LinearLayout) findViewById(R.id.LL_fb_login_container);
        LL_gmail_login_container = (LinearLayout) findViewById(R.id.LL_gmail_login_container);
        LL_email_login_container = (LinearLayout) findViewById(R.id.LL_email_login_container);
        LL_textContainer1 = (LinearLayout) findViewById(R.id.LL_textContainer1);
        LL_login_container = (LinearLayout) findViewById(R.id.LL_login_container);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        alertDialogBuilder = new AlertDialog.Builder(this);

        session = new SignIDAppSession(this);


    }


    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            //Getting the GoogleSignIn Task
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                //Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);

                //authenticating with firebase
                firebaseAuthWithGoogle(account);
                progress_bar.setVisibility(View.VISIBLE);
            } catch (ApiException e) {
                System.out.println("ERROR===" + e);
                // Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {

            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void displayMessage(Profile profile) {
        if (profile != null)
            System.out.println("DETAILS===123" + profile.getName());
    }

    protected void getUserDetails(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject json_object, GraphResponse response) {

                        System.out.println("DETAILS===" + json_object);
                        /*Intent intent = new Intent(FbLogingActivity.this,
                                UserProfile.class);
                        intent.putExtra("userProfile", json_object.toString());
                        startActivity(intent);*/
                    }

                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,location");
        data_request.setParameters(parameters);
        data_request.executeAsync();


    }


    private void getKeyHashes() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            System.out.println("ERROR=====" + e);
        } catch (NoSuchAlgorithmException e) {

            System.out.println("ERROR=====" + e);

        }
    }


    private void permissionControl() {
        if (ActivityCompat.checkSelfPermission(ContinueActivity.this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ContinueActivity.this, permissionsRequired[0])) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ContinueActivity.this);
                builder.setTitle("Permission");
                builder.setMessage("This app need Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(ContinueActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(permissionsRequired[0], false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ContinueActivity.this);
                builder.setTitle("Permissions");
                builder.setMessage("This Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(ContinueActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            }


            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(permissionsRequired[0], true);
            editor.commit();
        } else {
            //You already have the permission, just go ahead.
            //proceedAfterPermission();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(ContinueActivity.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                //proceedAfterPermission();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            //check if all permissions are granted
            boolean allgranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if (allgranted) {
                //proceedAfterPermission();
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(ContinueActivity.this, permissionsRequired[0])
                    ) {
                //  txtPermissions.setText("Permissions Required");
                AlertDialog.Builder builder = new AlertDialog.Builder(ContinueActivity.this);
                builder.setTitle("Permission");
                builder.setMessage("This app needs Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(ContinueActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                Toast.makeText(getBaseContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
            }
        }
    }


    private void showDialog(final String tag) {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_layout, null);


        LinearLayout LL_cancel_container = (LinearLayout) dialogView.findViewById(R.id.LL_cancel_container);
        LinearLayout LL_continue_container = (LinearLayout) dialogView.findViewById(R.id.LL_continue_container);


        TextView text1 = (TextView) dialogView.findViewById(R.id.text1);
        TextView text2 = (TextView) dialogView.findViewById(R.id.text2);
        TextView text3 = (TextView) dialogView.findViewById(R.id.text3);

        if (tag.equals("fb")) {
            text1.setText(getResources().getString(R.string.text1_fb));
            text2.setText(getResources().getString(R.string.text2_fb));
            text3.setText(getResources().getString(R.string.text3_fb));


        }
        if (tag.equals("gl")) {
            text1.setText(getResources().getString(R.string.text1_gl));
            text2.setText(getResources().getString(R.string.text2_gl));
            text3.setText(getResources().getString(R.string.text3_gl));
        }


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        LL_continue_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tag.equals("fb")) {
                    LoginManager.getInstance().logInWithReadPermissions(ContinueActivity.this, Arrays.asList("public_profile", "email"));
                    loginWithFacebook();
                }
                if (tag.equals("gl")) {
                    alertDialog.dismiss();
                    CommonLoadingDialog.showLoadingDialog(ContinueActivity.this, "Wait..");
                    signIn();


                }
            }
        });

        LL_cancel_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }


    private void getFbProfile(final AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                String strAccessToken = accessToken.getToken();

                fbUserID = accessToken.getUserId();
                fbToken = strAccessToken;
                try {
                    progress_bar.setVisibility(View.GONE);
                    str_facebookid = object.getString("id");
                    str_facebookname = object.getString("name");
                    fb_email = object.getString("email");
                    imageUrl = "https://graph.facebook.com/" + fbUserID + "/picture?type=large";
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                executeLoginWithFacebook(str_facebookid, str_facebookname, fb_email, imageUrl);
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link, email");
        request.setParameters(parameters);
        request.executeAsync();
    }


    private void executeLoginWithFacebook(String fb_id, String fb_user_name, String fb_email, final String imageUrl_fb) {
        try {
            String url = "http://www.signidapp.com/SignId/Facebook_Login.php?username=" + fb_user_name + "&email=" + fb_email + "&facebook_id=" + fb_id + "";
            url = url.replace(" ", "%20");
            System.out.println("URL===" + url);


            CommonLoadingDialog.showLoadingDialog(ContinueActivity.this, "Wait..");


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();


                            System.out.println("RESPONSE=====" + response);


                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                String message = jsonObject.getString("Message");


                                Gson gson = new Gson();
                                FbSignInResponse fbSignInResponse = gson.fromJson(response, FbSignInResponse.class);
                                FbSignInData fbSignInData = fbSignInResponse.getData();

                                    /*System.out.println("OUTPUT============EMAIL=="+fbSignInData.getEmail());
                                    System.out.println("OUTPUT============NAME=="+fbSignInData.getEmail());
                                    System.out.println("OUTPUT============FACEBOOK ID=="+fbSignInData.getFacebook_id());*/
                                session.setUser_name(fbSignInData.getUsername());
                                session.setUser_id(fbSignInData.getFacebook_id());
                                session.setUser_email(fbSignInData.getEmail());
                                session.setKeyUserImageUrl(imageUrl_fb);

                                System.out.println("OUTPUT============EMAIL==" + session.getUser_email());
                                System.out.println("OUTPUT============NAME==" + session.getUser_id());
                                System.out.println("OUTPUT============FACEBOOK ID==" + session.getUser_id());
                                System.out.println("OUTPUT============FACEBOOK IMAGE==" + session.getKeyUserImageUrl());


                              /*  startActivity(new Intent(ContinueActivity.this, SettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                finish();*/
                                showLoginSuccess("Login Successful");


                                //  Toast.makeText(ContinueActivity.this, message, Toast.LENGTH_SHORT).show();


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();

                            System.out.println("ERROR======" + error.toString());
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ContinueActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex.toString());
        }
    }


    private void executeLoginWithGoogle(String username, String email, String google_plus_id) {


        try {
            String url = "http://www.signidapp.com/SignId/Login_With_Google_Plus.php?username=" + username + "&email=" + email + "&google_plus_id=" + google_plus_id + "";
            url = url.replace(" ", "%20");
            System.out.println("URL===" + url);


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // CommonLoadingDialog.closeLoadingDialog();

                            progress_bar.setVisibility(View.GONE);
                            LL_login_container.setEnabled(true);
                            System.out.println("RESPONSE=====" + response);


                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                //String status= jsonObject.getString("Status");
                                String message = jsonObject.getString("Message");


                                Gson gson = new Gson();
                                FbSignInResponse fbSignInResponse = gson.fromJson(response, FbSignInResponse.class);
                                FbSignInData fbSignInData = fbSignInResponse.getData();

                                    /*System.out.println("OUTPUT============EMAIL=="+fbSignInData.getEmail());
                                    System.out.println("OUTPUT============NAME=="+fbSignInData.getEmail());
                                    System.out.println("OUTPUT============FACEBOOK ID=="+fbSignInData.getFacebook_id());*/
                                session.setUser_name(fbSignInData.getUsername());
                                session.setUser_id(fbSignInData.getFacebook_id());
                                session.setUser_email(fbSignInData.getEmail());
                                session.setKeyUserImageUrl(user.getPhotoUrl().toString());

                                System.out.println("OUTPUT============EMAIL==" + session.getUser_email());
                                System.out.println("OUTPUT============NAME==" + session.getUser_id());
                                System.out.println("OUTPUT============GOOGLE PLUS ID==" + session.getUser_id());
                                System.out.println("OUTPUT============IAMGEGE==" + session.getKeyUserImageUrl());


                                showLoginSuccess("Login Successful");


                                //Toast.makeText(ContinueActivity.this, message, Toast.LENGTH_SHORT).show();


                            } catch (Exception ex) {
                                //CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  CommonLoadingDialog.closeLoadingDialog();

                            System.out.println("ERROR======" + error.toString());
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ContinueActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex.toString());
        }
    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        //Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        //getting the auth credential
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        //Now using firebase we are signing in the user here
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //  Log.d(TAG, "signInWithCredential:success");
                            user = mAuth.getCurrentUser();


                            System.out.println("USER DATA EMAIL====" + user.getEmail());
                            System.out.println("USER DATA NAME====" + user.getDisplayName());
                            System.out.println("USER DATA ID====" + user.getUid());
                            System.out.println("IMAGE URL===" + user.getPhotoUrl());

                            executeLoginWithGoogle(user.getDisplayName(), user.getEmail(), user.getUid());


                            // Toast.makeText(getApplicationContext(), "User Signed In", Toast.LENGTH_SHORT).show();
                        } else {
                            // If sign in fails, display a message to the user.
                            // Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }


    private void signIn() {
        //getting the google signin intent
        progress_bar.setVisibility(View.VISIBLE);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        progress_bar.setVisibility(View.GONE);
        //starting the activity for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void showLoginSuccess(String msg) {
        alertDialogBuilder2 = new AlertDialog.Builder(ContinueActivity.this);
        LayoutInflater LayoutInflater = getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dailog_validation_layout, null);
        LinearLayout LL_ok = (LinearLayout) dialogView.findViewById(R.id.LL_ok);
        TextView textMessage = (TextView) dialogView.findViewById(R.id.textMessage);
        TextView okBtn = (TextView) dialogView.findViewById(R.id.okBtn);

        textMessage.setText(msg);


        alertDialogBuilder.setView(dialogView);
        alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        alertDialog.show();


        LL_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                String pkg = "";

                InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                for (int i = 0; i < imeManager.getEnabledInputMethodList().size(); i++) {
                    pkg = imeManager.getEnabledInputMethodList().get(i).getPackageName();

                    System.out.println("PACKAGE===========" + pkg);
                    if (pkg.equals("com.app.signidapp")) {
                        break;
                    }


                }

                // System.out.println("PACKAGE==========="+pkg);

                if (pkg.equals("com.app.signidapp")) {
                    startActivity(new Intent(ContinueActivity.this, SettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                } else {
                    startActivity(new Intent(ContinueActivity.this, KeyboardSettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                }


            }
        });
    }

    protected void onUserLeaveHint() {
        super.onUserLeaveHint();

//        ActivityManager am = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
//        if(am != null) {
//            List<ActivityManager.AppTask> tasks = am.getAppTasks();
//            if (tasks != null && tasks.size() > 0) {
//
//                tasks.get(0).setExcludeFromRecents(true);
//
//               finish();
//            }
//        }

    }
}
