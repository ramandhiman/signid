// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FeedbackActivity_ViewBinding implements Unbinder {
  private FeedbackActivity target;

  @UiThread
  public FeedbackActivity_ViewBinding(FeedbackActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FeedbackActivity_ViewBinding(FeedbackActivity target, View source) {
    this.target = target;

    target.backBtn = Utils.findRequiredViewAsType(source, R.id.backBtn, "field 'backBtn'", ImageView.class);
    target.feedbackSendText = Utils.findRequiredViewAsType(source, R.id.feedbackSendText, "field 'feedbackSendText'", TextView.class);
    target.inputFeedBack = Utils.findRequiredViewAsType(source, R.id.inputFeedBack, "field 'inputFeedBack'", EditText.class);
    target.inputEmail = Utils.findRequiredViewAsType(source, R.id.inputEmail, "field 'inputEmail'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FeedbackActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backBtn = null;
    target.feedbackSendText = null;
    target.inputFeedBack = null;
    target.inputEmail = null;
  }
}
