// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class KeyboardActivity_ViewBinding implements Unbinder {
  private KeyboardActivity target;

  @UiThread
  public KeyboardActivity_ViewBinding(KeyboardActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public KeyboardActivity_ViewBinding(KeyboardActivity target, View source) {
    this.target = target;

    target.backBtn = Utils.findRequiredViewAsType(source, R.id.backBtn, "field 'backBtn'", ImageView.class);
    target.text_registerNow = Utils.findRequiredViewAsType(source, R.id.text_registerNow, "field 'text_registerNow'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    KeyboardActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backBtn = null;
    target.text_registerNow = null;
  }
}
