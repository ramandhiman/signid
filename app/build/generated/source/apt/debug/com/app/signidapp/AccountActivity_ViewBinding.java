// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AccountActivity_ViewBinding implements Unbinder {
  private AccountActivity target;

  @UiThread
  public AccountActivity_ViewBinding(AccountActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AccountActivity_ViewBinding(AccountActivity target, View source) {
    this.target = target;

    target.backBtn = Utils.findRequiredViewAsType(source, R.id.backBtn, "field 'backBtn'", ImageView.class);
    target.text_user_name = Utils.findRequiredViewAsType(source, R.id.text_user_name, "field 'text_user_name'", TextView.class);
    target.LL_sign_up_container = Utils.findRequiredViewAsType(source, R.id.LL_sign_up_container, "field 'LL_sign_up_container'", LinearLayout.class);
    target.imageViewAc = Utils.findRequiredViewAsType(source, R.id.imageViewAc, "field 'imageViewAc'", CircleImageView.class);
    target.textLoginLogoutStatus = Utils.findRequiredViewAsType(source, R.id.textLoginLogoutStatus, "field 'textLoginLogoutStatus'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AccountActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backBtn = null;
    target.text_user_name = null;
    target.LL_sign_up_container = null;
    target.imageViewAc = null;
    target.textLoginLogoutStatus = null;
  }
}
