// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SettingActivity_ViewBinding implements Unbinder {
  private SettingActivity target;

  @UiThread
  public SettingActivity_ViewBinding(SettingActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SettingActivity_ViewBinding(SettingActivity target, View source) {
    this.target = target;

    target.LL_keybord_status_container = Utils.findRequiredViewAsType(source, R.id.LL_keybord_status_container, "field 'LL_keybord_status_container'", LinearLayout.class);
    target.LL_lockSet = Utils.findRequiredViewAsType(source, R.id.LL_lockSet, "field 'LL_lockSet'", LinearLayout.class);
    target.LL_changePasscode = Utils.findRequiredViewAsType(source, R.id.LL_changePasscode, "field 'LL_changePasscode'", LinearLayout.class);
    target.LL_signature_setting = Utils.findRequiredViewAsType(source, R.id.LL_signature_setting, "field 'LL_signature_setting'", LinearLayout.class);
    target.LL_signature_account = Utils.findRequiredViewAsType(source, R.id.LL_signature_account, "field 'LL_signature_account'", LinearLayout.class);
    target.LL_feedback = Utils.findRequiredViewAsType(source, R.id.LL_feedback, "field 'LL_feedback'", LinearLayout.class);
    target.LL_support = Utils.findRequiredViewAsType(source, R.id.LL_support, "field 'LL_support'", LinearLayout.class);
    target.LL_about = Utils.findRequiredViewAsType(source, R.id.LL_about, "field 'LL_about'", LinearLayout.class);
    target.backBtn = Utils.findRequiredViewAsType(source, R.id.backBtn, "field 'backBtn'", ImageView.class);
    target.keyBoardStatusText = Utils.findRequiredViewAsType(source, R.id.keyBoardStatusText, "field 'keyBoardStatusText'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SettingActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.LL_keybord_status_container = null;
    target.LL_lockSet = null;
    target.LL_changePasscode = null;
    target.LL_signature_setting = null;
    target.LL_signature_account = null;
    target.LL_feedback = null;
    target.LL_support = null;
    target.LL_about = null;
    target.backBtn = null;
    target.keyBoardStatusText = null;
  }
}
