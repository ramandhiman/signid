// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class KeyboardChoosing_ViewBinding implements Unbinder {
  private KeyboardChoosing target;

  @UiThread
  public KeyboardChoosing_ViewBinding(KeyboardChoosing target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public KeyboardChoosing_ViewBinding(KeyboardChoosing target, View source) {
    this.target = target;

    target.keyboard_choose_btn = Utils.findRequiredViewAsType(source, R.id.keyboard_choose_btn, "field 'keyboard_choose_btn'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    KeyboardChoosing target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.keyboard_choose_btn = null;
  }
}
